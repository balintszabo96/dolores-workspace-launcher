set vmname="c:\Users\balin\Documents\Virtual Machines\Windows 10 x64\Windows 10 x64.vmx"
set snapshot="test7"
set guestuser="balin"
set guestpass="123"
set hostpath="d:\univ\dolores\dolores\Teddy\dist\DoloresInstaller.exe"
set guestpath="c:\test\DoloresInstaller.exe"

vmrun -T ws -gu %guestuser% -gp %guestpass% revertToSnapshot %vmname% %snapshot%
vmrun -T ws -gu %guestuser% -gp %guestpass% start %vmname%
vmrun -T ws -gu %guestuser% -gp %guestpass% CopyFileFromHostToGuest %vmname% %hostpath% %guestpath%
::vmrun -T ws -gu %guestuser% -gp %guestpass% runProgramInGuest %vmname% %guestpath%