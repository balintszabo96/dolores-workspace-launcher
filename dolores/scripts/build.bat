@echo off
if %2%==32 set dotnet_platform="x86"
if %2%==32 set cplusplus_platform="Win32"
if %2%==64 set dotnet_platform="x64"
if %2%==64 set cplusplus_platform="x64"

::msbuild d:\univ\dolores\dolores\Bernard\Bernard.vcxproj /p:Configuration=%1 /p:Platform=%cplusplus_platform% /t:Clean,Build
msbuild d:\univ\dolores\dolores\Dolores\Dolores.csproj /p:Configuration=%1 /p:Platform=%dotnet_platform% /t:Clean,Build
msbuild d:\univ\dolores\dolores\DoloresWPF\DoloresWPF.csproj /p:Configuration=%1 /p:Platform=%dotnet_platform% /t:Clean,Build
msbuild d:\univ\dolores\dolores\DrFord\DrFord.csproj /p:Configuration=%1 /p:Platform=%dotnet_platform% /t:Clean,Build


pyinstaller d:\univ\dolores\dolores\Teddy\Teddy.spec