#include "MyService.hpp"
#include "dolores.hpp"
#include <thread>
#include <chrono>
#include <atomic>

std::shared_ptr<Configuration> gCfg;
std::shared_ptr<FeedbackManager> gFm;

MyService::MyService(
    __in const std::string &name
) : Service(name, true, true, false), AppThreadHandle(INVALID_HANDLE_VALUE), FeedbackThreadHandle(INVALID_HANDLE_VALUE), ExitCode(1)
{
    DWORD status = 0;
    WCHAR cwd[MAX_PATH] = { 0 };

    status = GetModuleFileNameW(NULL, cwd, MAX_PATH);
    NarrativeLogger::GetInstance()->Info(status, L"GetModuleFileNameExW", cwd);
    status = PathRemoveFileSpecW(cwd);
    NarrativeLogger::GetInstance()->Info(status, L"PathRemoveFileSpecW", cwd);
    status = SetCurrentDirectoryW(cwd);
    NarrativeLogger::GetInstance()->Info(status, L"SetCurrentDirectoryW", L"");

    gCfg = std::make_shared<Configuration>();
    gFm = std::make_shared<FeedbackManager>();
}


MyService::~MyService()
{
    if (AppThreadHandle != INVALID_HANDLE_VALUE)
    {
        CloseHandle(AppThreadHandle);
    }
    if (FeedbackThreadHandle != INVALID_HANDLE_VALUE)
    {
        CloseHandle(FeedbackThreadHandle);
    }
}

void MyService::AppThreadFunction()
{
    NarrativeLogger::GetInstance()->Info(0, L"Service::AppThreadFunction", L"In appthread");
    try
    {
        NarrativeLogger::GetInstance()->Info(L"ListeningPort: " + std::to_wstring(gCfg->ListengingPort) + L" SendingPort: " + std::to_wstring(gCfg->SendingPort));
        TelegraphServer server;
        server.Listen();
    }
    catch (std::exception &ex)
    {
        NarrativeLogger::GetInstance()->Exception(ex.what());
        return;
    }
}

void MyService::FeedbackThreadFunction()
{

    while (true)
    {
        gFm->CheckQueue();
        std::this_thread::sleep_for(std::chrono::seconds(60));
    }
}

void MyService::onStart(DWORD argc, LPSTR * argv)
{
    setStateRunning();

    AppThreadHandle = CreateThread(
        nullptr,
        0,
        (LPTHREAD_START_ROUTINE)&AppThreadFunction,
        (LPVOID)this,
        0,
        nullptr
    );

    if (AppThreadHandle == INVALID_HANDLE_VALUE)
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"CreateThread", L"Failed to create the application thread");
        setStateStopped(1);
        return;
    }

    FeedbackThreadHandle = CreateThread(
        nullptr,
        0,
        (LPTHREAD_START_ROUTINE)&FeedbackThreadFunction,
        (LPVOID)this,
        0,
        nullptr
    );

    if (FeedbackThreadHandle == INVALID_HANDLE_VALUE)
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"CreateThread", L"Failed to create the feedback thread");
        setStateStopped(1);
        return;
    }
}

void MyService::onStop()
{
    DWORD status = WaitForSingleObject(&AppThreadHandle, INFINITE);
    if (status == WAIT_FAILED)
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"WaitForSingleObject", L"Failed to wait the application thread");
    }

    status = WaitForSingleObject(&FeedbackThreadHandle, INFINITE);
    if (status == WAIT_FAILED)
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"WaitForSingleObject", L"Failed to wait the feedback thread");
    }

    // exitCode_ should be set by the application thread on exit
    setStateStopped(ExitCode);
}
