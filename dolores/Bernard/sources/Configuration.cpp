#include "Configuration.hpp"
#include "document.h"
#include "istreamwrapper.h"
#include <fstream>
#include "NarrativeLogger.hpp"

Configuration::Configuration()
{
    try
    {
        std::ifstream ifs(ConfigPath);
        rapidjson::IStreamWrapper isw(ifs);
        rapidjson::Document doc;
        doc.ParseStream(isw);

        ListengingPort = doc["listeningPort"].GetInt();
        SendingPort = doc["sendingPort"].GetInt();
        ifs.close();
    }
    catch(std::exception &e)
    {
        NarrativeLogger::GetInstance()->Exception(e.what());
    }
}


Configuration::~Configuration()
{
}
