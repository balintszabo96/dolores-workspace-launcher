#include "RequestSavePreset.hpp"
#include "SendAllPresets.hpp"

RequestSavePreset::RequestSavePreset(JsonDocument & doc)
{
    auto presetDoc = doc[L"Details"].GetObject();

    Preset newPreset(
        presetDoc[L"Id"].GetInt64(),
        presetDoc[L"Name"].GetString()
    );

    if (presetDoc.HasMember(L"ProcInfos"))
    {
        auto docProcInfo = presetDoc[L"ProcInfos"].GetArray();
        for (ULONG itProcInfo = 0; itProcInfo < docProcInfo.Size(); itProcInfo++)
        {
            ProcInfo procInfo(
                docProcInfo[itProcInfo][L"Id"].GetInt64(),
                docProcInfo[itProcInfo][L"Name"].GetString(),
                docProcInfo[itProcInfo][L"ExecutablePath"].GetString(),
                docProcInfo[itProcInfo][L"CommandLine"].GetString()
            );

            if (docProcInfo[itProcInfo].HasMember(L"Windows"))
            {
                auto docWindow = docProcInfo[itProcInfo][L"Windows"].GetArray();
                for (ULONG itWindow = 0; itWindow < docWindow.Size(); itWindow++)
                {
                    RECT rectangle;
                    rectangle.left = (LONG)docWindow[itWindow][L"Rectangle"][L"Left"].GetInt64();
                    rectangle.top = (LONG)docWindow[itWindow][L"Rectangle"][L"Top"].GetInt64();
                    rectangle.right = (LONG)docWindow[itWindow][L"Rectangle"][L"Right"].GetInt64();
                    rectangle.bottom = (LONG)docWindow[itWindow][L"Rectangle"][L"Bottom"].GetInt64();

                    Window window(
                        docWindow[itWindow][L"Id"].GetInt64(),
                        rectangle,
                        docWindow[itWindow][L"IsVisible"].GetBool(),
                        docWindow[itWindow][L"Title"].GetString(),
                        docWindow[itWindow][L"IsActive"].GetBool()
                    );

                    procInfo.Windows.push_back(window);
                }
            }

            newPreset.ProcInfos.push_back(procInfo);
        }
    }
    PresetToSave = newPreset;
}

bool RequestSavePreset::IsMatching(std::wstring & NewType)
{
    return L"requestsavepreset" == NewType;
}

bool RequestSavePreset::Execute()
{
    NarrativeLogger::GetInstance()->Info(L"Executing RequestSavePreset");
    SqliteCRUD msc;
    auto existingPreset = msc.SelectPresets(L"name", PresetToSave.Name, true);
    if (!existingPreset.empty())
    {
        msc.DeletePresetById(existingPreset[0].Id);
    }
    msc.InsertConfig(PresetToSave);
    auto presets = msc.SelectPresets(L"id", L"%", true);
    SendAllPresets SendAllPresets(presets);
    SendAllPresets.Send();

    return true;
}
