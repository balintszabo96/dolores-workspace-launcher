#pragma once
#include <string>
#include <mutex>
#include "FeedbackConfig.hpp"

class FeedbackManager
{
public:
    FeedbackManager();
    ~FeedbackManager() = default;    
    void CheckQueue();
    bool IsFeedbackEnabled();
    bool SetFeedbackStatus(bool status);

    FeedbackManager(const FeedbackManager&) = delete;
    FeedbackManager& operator = (const FeedbackManager&) = delete;
private:
    void RegisterProduct();
    int SubmitMessage(std::string message);

    std::shared_ptr<FeedbackConfig> feedbackConfig;
    std::mutex feedbackConfigLock;
    std::string ticketController = "/api/Ticket";
    std::string authenticationController = "/api/Authentication";
    std::string feedbackConfigFile = "Feedback.config";
    std::string machineGuidSubKey = "SOFTWARE\\Microsoft\\Cryptography";
    std::string machineGuidValueName = "MachineGuid";
    std::string feedbackFolder = "feedback";
    std::string ticketPattern = "feedback/*.ticket";
};