#include "TelegraphServer.hpp"
#include <winsock2.h>
#include <ws2tcpip.h>
#include "NarrativeLogger.hpp"
#include "PacketFactory.hpp"

#pragma comment(lib, "Ws2_32.lib")

TelegraphServer::TelegraphServer()
{
}


TelegraphServer::~TelegraphServer()
{
}

void TelegraphServer::Listen()
{
    WSADATA wsData;
    WORD ver = MAKEWORD(2, 2);
    SOCKET serverSocket;
    SOCKET clientSocket;
    sockaddr_in serverAddr;
    sockaddr_in clientAddr;
    ULONG localhost = 0;    
    WCHAR buffer[MAX_BUFFER_CHARACTERS_PLUS_ZERO];
    DWORD bytesRead = 0;
    int clientSize = 0;
    int port = 0;
    std::wstring recieved;
    bool isSocketStillActive = true;

    int status = WSAStartup(ver, &wsData);
    if (!SUCCESS(status))
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"WSAStartup", L"Cannot initialize socket");
        return;
    }
    NarrativeLogger::GetInstance()->Info(L"WSAStartup succeeded");

    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == INVALID_SOCKET)
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"socket", L"Creating listening socket failed");
        goto cleanup;
    }
    NarrativeLogger::GetInstance()->Info(L"Created listening socket");
    
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(gCfg->ListengingPort);
    status = inet_pton(AF_INET, "127.0.0.1", &localhost);
    if (status != 1)
    {
        NarrativeLogger::GetInstance()->Error(status, L"inet_pton", L"Could not convert localhost");
        goto cleanup;
    }
    serverAddr.sin_addr.S_un.S_addr = localhost;

    status = bind(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr));
    if (!SUCCESS(status))
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"bind", L"Bind failed");
        goto cleanup;
    }
    NarrativeLogger::GetInstance()->Info(L"Binding done");

    status = listen(serverSocket, SOMAXCONN);
    if (!SUCCESS(status))
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"listen", L"Listen failed");
        goto cleanup;
    }
    NarrativeLogger::GetInstance()->Info(L"Listening done");

    while (true)
    {        
        clientSize = sizeof(clientAddr);
        NarrativeLogger::GetInstance()->Info(L"Accepting new client");
        clientSocket = accept(serverSocket, (sockaddr*)&clientAddr, &clientSize);
        if (clientSocket == INVALID_SOCKET)
        {
            NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"accept", L"Accepting connection failed");
            goto cleanup;
        }
        
        port = clientAddr.sin_port;
        NarrativeLogger::GetInstance()->Info(L"Accepted connection on port " + std::to_wstring(port));
        while (true)
        {
            ZeroMemory(buffer, MAX_BUFFER_BYTES_PLUS_ZERO);
            NarrativeLogger::GetInstance()->Info(L"Waiting for new message");
            bytesRead = recv(clientSocket, (char*)buffer, MAX_BUFFER_BYTES, 0);
            if (bytesRead == SOCKET_ERROR)
            {
                NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"recv", L"Socket error occured");
                break;
            }

            recieved += buffer;
            if (bytesRead < MAX_BUFFER_BYTES)
            {
                isSocketStillActive = Handle(recieved);
                recieved = L"";
                if (!isSocketStillActive)
                {
                    closesocket(clientSocket);
                    break;
                }
            }            
        }        
    }
cleanup:
    WSACleanup();
}

bool TelegraphServer::Handle(std::wstring & Message)
{
    JsonDocument doc;
    std::wstring type;
    bool hasError;
    std::shared_ptr<IncomingPacket> packet;

    if (Message.length() == 0)
    {
        return false;
    }

    try
    {
        hasError = doc.Parse(Message.c_str()).HasParseError();

        if (hasError)
        {
            NarrativeLogger::GetInstance()->Error(L"Parse error:\nMessage length: " + std::to_wstring(Message.length()) + L"\nMessage: " + Message);
            throw std::exception("Parse error in Handle");
        }

        type = doc[L"Type"].GetString();

        NarrativeLogger::GetInstance()->Info(L"Received command: " + type);

        packet = PacketFactory::CreateMatchingPacket(type, doc);
        return (packet) ? packet->Execute() : true;
    }
    catch (const std::exception &ex)
    {
        NarrativeLogger::GetInstance()->Exception(ex.what());
    }

    return true;
}
