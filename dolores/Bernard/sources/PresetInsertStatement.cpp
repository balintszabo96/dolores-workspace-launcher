#include "PresetInsertStatement.hpp"

PresetInsertStatement::PresetInsertStatement(sqlite3 * Database) : InsertStatementBase(Database, L"INSERT INTO preset VALUES(@id, @name)")
{
}

void PresetInsertStatement::Bind(std::wstring & Name)
{
    Reset();
    BindWstring(Statement, 2, Name);
}
