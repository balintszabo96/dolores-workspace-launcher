#pragma once
#include "IncomingPacket.hpp"

class PacketFactory
{
public:
    PacketFactory() = default;
    ~PacketFactory() = default;

    static std::shared_ptr<IncomingPacket> CreateMatchingPacket(std::wstring& NewType, JsonDocument& doc);
};

