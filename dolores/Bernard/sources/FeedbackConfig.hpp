#pragma once
#include <string>

class FeedbackConfig
{
public:
    FeedbackConfig(std::string FeedbackFileName);
    FeedbackConfig() = default;
    ~FeedbackConfig() = default;

    void Dump(std::string FeedbackFileName);

    bool IsEnabled;
    std::string Username;
    std::string Password;
    std::string Url;
};

