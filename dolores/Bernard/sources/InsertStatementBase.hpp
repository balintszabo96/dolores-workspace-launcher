#pragma once
#include "StatementBase.hpp"

class InsertStatementBase :
    public StatementBase
{
public:
    InsertStatementBase(sqlite3 *Database, std::wstring Query);
    InsertStatementBase() = default;
    ~InsertStatementBase() = default;

    sqlite3_int64 Execute();
};

