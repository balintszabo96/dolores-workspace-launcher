#include "OutgoingPacket.hpp"

OutgoingPacket::OutgoingPacket(std::wstring Type)
{
    this->Type = Type;
}

void OutgoingPacket::Send()
{
    auto stringBuffer = Serialize();
    TelegraphClient client;
    client.Send(stringBuffer.GetString());
}
