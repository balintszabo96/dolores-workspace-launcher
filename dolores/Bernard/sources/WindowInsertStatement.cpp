#include "WindowInsertStatement.hpp"



WindowInsertStatement::WindowInsertStatement(sqlite3 * Database) : InsertStatementBase(Database, L"INSERT INTO window VALUES (@id, @procinfo_id, @is_visible, @title, @left, @top, @right, @bottom, @is_active);")
{
}

void WindowInsertStatement::Bind(sqlite3_int64 ProcInfoId, Window &Wnd)
{
    Reset();
    BindInt64(Statement, 2, ProcInfoId);
    BindInt(Statement, 3, Wnd.IsVisible);
    BindWstring(Statement, 4, Wnd.Title);    
    BindInt64(Statement, 5, Wnd.Rectangle.left);
    BindInt64(Statement, 6, Wnd.Rectangle.top);
    BindInt64(Statement, 7, Wnd.Rectangle.right);
    BindInt64(Statement, 8, Wnd.Rectangle.bottom);
    BindInt(Statement, 9, Wnd.IsActive);
}