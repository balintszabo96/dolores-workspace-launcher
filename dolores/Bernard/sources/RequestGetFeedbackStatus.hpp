#pragma once
#include "IncomingPacket.hpp"

class RequestGetFeedbackStatus :
    public IncomingPacket
{
public:
    RequestGetFeedbackStatus() = default;
    ~RequestGetFeedbackStatus() = default;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
};

