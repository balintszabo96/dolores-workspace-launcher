#include "InsertStatementBase.hpp"


InsertStatementBase::InsertStatementBase(sqlite3 * Database, std::wstring Query) : StatementBase(Database, Query)
{
}

sqlite3_int64 InsertStatementBase::Execute()
{
    auto status = sqlite3_step(Statement);
    if (status != SQLITE_DONE)
    {
        throw std::exception(("sqlite3_step failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database) + " Statement: " + sqlite3_sql(Statement)).c_str());
    }
    return sqlite3_last_insert_rowid(Database);
}

