#pragma once
#include "dolores.hpp"
#include "TelegraphClient.hpp"

class OutgoingPacket
{
public:
    OutgoingPacket(std::wstring Type);
    OutgoingPacket() = default;
    ~OutgoingPacket() = default;

    void Send();
protected:
    std::wstring Type;
    virtual rapidjson::GenericStringBuffer<rapidjson::UTF16<>> Serialize() = 0;
};

