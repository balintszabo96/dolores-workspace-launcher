#pragma once
#include "SelectStatementBase.hpp"
#include "Preset.hpp"
#include "ProcInfo.hpp"

class PresetSelectStatement :
    public SelectStatementBase
{
public:
    PresetSelectStatement(sqlite3 *Database, std::wstring Field, std::wstring Like);
    PresetSelectStatement() = default;
    ~PresetSelectStatement() = default;

    std::vector<Preset> Execute(bool Recursive);
private:
    std::vector<ProcInfo> SelectProcInfosForPresetId(sqlite3_int64 PresetId, bool Recursive);
};

