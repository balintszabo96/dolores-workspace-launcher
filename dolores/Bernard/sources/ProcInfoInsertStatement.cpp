#include "ProcInfoInsertStatement.hpp"

ProcInfoInsertStatement::ProcInfoInsertStatement(sqlite3 * Database) : InsertStatementBase(Database, L"INSERT INTO procinfo VALUES (@id, @preset_id, @name, @executable_path, @command_line)")
{
}

void ProcInfoInsertStatement::Bind(sqlite3_int64 PresetId, ProcInfo & PInfo)
{
    Reset();
    BindInt64(Statement, 2, PresetId);
    BindWstring(Statement, 3, PInfo.Name);
    BindWstring(Statement, 4, PInfo.ExecutablePath);
    BindWstring(Statement, 5, PInfo.CommandLine);
}
