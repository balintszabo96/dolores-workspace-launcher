#include "ServiceError.hpp"

ServiceError::ServiceError(std::wstring & Message) : OutgoingPacket(L"serviceerror")
{
    this->Message = Message;
}

rapidjson::GenericStringBuffer<rapidjson::UTF16<>> ServiceError::Serialize()
{
    rapidjson::GenericStringBuffer<rapidjson::UTF16<>> stringBuffer;
    rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF16<>>, rapidjson::UTF16<>, rapidjson::UTF16<>> writer(stringBuffer);

    writer.StartObject();
    
    writer.Key(L"Type");
    writer.String(Type.c_str());

    writer.Key(L"Details");
    writer.String(Message.c_str());

    writer.EndObject();

    return stringBuffer;
}
