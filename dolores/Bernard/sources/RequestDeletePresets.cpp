#include "RequestDeletePresets.hpp"
#include "SendAllPresets.hpp"

RequestDeletePresets::RequestDeletePresets(JsonDocument & doc)
{
    DeletePresetId = doc[L"DeletePresetId"].GetInt64();
}

bool RequestDeletePresets::IsMatching(std::wstring & NewType)
{
    return std::wstring(L"requestdeletepreset") == NewType;
}

bool RequestDeletePresets::Execute()
{
    NarrativeLogger::GetInstance()->Info(L"Executing RequestDeletePresets");
    SqliteCRUD msc;
    msc.DeletePresetById(DeletePresetId);
    auto presets = msc.SelectPresets(L"id", L"%", true);
    SendAllPresets SendAllPresets(presets);
    SendAllPresets.Send();

    return true;
}
