#include "ProcInfoSelectStatement.hpp"
#include "WindowSelectStatement.hpp"

ProcInfoSelectStatement::ProcInfoSelectStatement(sqlite3 * Database, std::wstring Field, std::wstring Like)
    :SelectStatementBase(Database, L"procinfo", Field, Like)
{
}

std::vector<ProcInfo> ProcInfoSelectStatement::Execute(bool Recursive)
{
    int status = SQLITE_ERROR;
    std::vector<ProcInfo> procInfos;

    while ((status = sqlite3_step(Statement)) == SQLITE_ROW)
    {
        ProcInfo pInfo(
            sqlite3_column_int64(Statement, 0),
            (const wchar_t*)sqlite3_column_text16(Statement, 2),
            (const wchar_t*)sqlite3_column_text16(Statement, 3),
            (const wchar_t*)sqlite3_column_text16(Statement, 4)
        );
        pInfo.Windows = (Recursive) ? SelectWindowsForProcInfoId(pInfo.Id) : std::vector<Window>();
        procInfos.push_back(pInfo);
    }
    if (status != SQLITE_DONE)
    {
        throw std::exception(("sqlite3_step failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database) + " Statement: " + sqlite3_sql(Statement)).c_str());
    }

    return procInfos;
}

std::vector<Window> ProcInfoSelectStatement::SelectWindowsForProcInfoId(sqlite3_int64 ProcInfoId)
{
    WindowSelectStatement* windowSelectStat = new WindowSelectStatement(Database, L"procinfo_id", std::to_wstring(ProcInfoId));
    auto windows = windowSelectStat->Execute();
    delete windowSelectStat;
    return windows;
}
