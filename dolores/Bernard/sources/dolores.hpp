#pragma once

#define NOMINMAX

#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include <windows.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <istream>
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <map>
#include <thread>
#include "shlwapi.h"
#include "Configuration.hpp"
#include <sqlite3.h>
#include "writer.h"
#include "stringbuffer.h"
#include "encodings.h"
#include "document.h"
#include "FeedbackManager.hpp"

#define SUCCESS(Status) (Status == 0)

#define MAX_BUFFER_CHARACTERS 4096
#define MAX_BUFFER_CHARACTERS_PLUS_ZERO (MAX_BUFFER_CHARACTERS + 1)
#define MAX_BUFFER_BYTES (MAX_BUFFER_CHARACTERS * sizeof(WCHAR))
#define MAX_BUFFER_BYTES_PLUS_ZERO (MAX_BUFFER_CHARACTERS_PLUS_ZERO * sizeof(WCHAR))

extern std::shared_ptr<Configuration> gCfg;
extern std::shared_ptr<FeedbackManager> gFm;

typedef rapidjson::GenericDocument<rapidjson::UTF16<wchar_t>, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator>, rapidjson::CrtAllocator> JsonDocument;