#pragma once
#include "dolores.hpp"
#include "NarrativeLogger.hpp"

class TelegraphClient
{
public:
    TelegraphClient();
    ~TelegraphClient();
    void Send(std::wstring Message);
};

