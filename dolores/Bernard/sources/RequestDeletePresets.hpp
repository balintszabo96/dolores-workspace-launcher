#pragma once
#include "IncomingPacket.hpp"

class RequestDeletePresets : public IncomingPacket
{
public:
    RequestDeletePresets(JsonDocument &doc);
    RequestDeletePresets() = default;
    ~RequestDeletePresets() = default;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;

private:
    sqlite3_int64 DeletePresetId;
};