#pragma once
#include "IncomingPacket.hpp"

class RequestAllPresets : public IncomingPacket
{
public:
    RequestAllPresets() = default;
    ~RequestAllPresets() = default;
    
    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
};

