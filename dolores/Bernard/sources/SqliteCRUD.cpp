#include "SqliteCRUD.hpp"
#include "NarrativeLogger.hpp"


SqliteCRUD::SqliteCRUD()
{
    char *errMsg = nullptr;

    auto status = sqlite3_open16(DatabasePath.c_str(), &Database);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_open failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
    NarrativeLogger::GetInstance()->Info(L"Opened database");

    status = sqlite3_exec(Database, "PRAGMA foreign_keys = on", nullptr, nullptr, &errMsg);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_exec failed with Status: " + std::to_string(status) + " Message: " + errMsg).c_str());
    }
}

SqliteCRUD::~SqliteCRUD()
{
    sqlite3_close(Database);
    NarrativeLogger::GetInstance()->Info(L"Closed database");
}

sqlite3_int64 SqliteCRUD::InsertConfig(Preset &NewPreset)
{
    sqlite3_int64 presetId = InsertPreset(NewPreset.Name);
    for (auto &procInfo : NewPreset.ProcInfos)
    {
        sqlite3_int64 procInfoId = InsertProcInfo(presetId, procInfo);
        for (auto &window : procInfo.Windows)
        {
            InsertWindow(procInfoId, window);
        }
    }
    return presetId;
}

sqlite3_int64 SqliteCRUD::InsertPreset(std::wstring PresetName)
{
    PresetInsertStatement presetInsertStat(Database);
    presetInsertStat.Bind(PresetName);
    return presetInsertStat.Execute();
}

sqlite3_int64 SqliteCRUD::InsertProcInfo(sqlite3_int64 PresetId, ProcInfo & PInfo)
{
    ProcInfoInsertStatement procInfoInsertStat(Database);
    procInfoInsertStat.Bind(PresetId, PInfo);
    return procInfoInsertStat.Execute();
}

sqlite3_int64 SqliteCRUD::InsertWindow(sqlite3_int64 ProcInfoId, Window & Wnd)
{
    WindowInsertStatement windowInsertStat(Database);
    windowInsertStat.Bind(ProcInfoId, Wnd);
    return windowInsertStat.Execute();
}

void SqliteCRUD::DeletePresetById(sqlite3_int64 PresetId)
{
    GenDeleteStatement presetDeleteStat(Database, L"preset");
    presetDeleteStat.Bind(PresetId);
    presetDeleteStat.Execute();
}

std::vector<Window> SqliteCRUD::SelectWindows(std::wstring Field, std::wstring Like)
{
    WindowSelectStatement WindowSelectStat(Database, Field, Like);
    return WindowSelectStat.Execute();
}

std::vector<ProcInfo> SqliteCRUD::SelectProcInfos(std::wstring Field, std::wstring Like, bool Recursive)
{
    ProcInfoSelectStatement procInfoSelectStat(Database, Field, Like);
    return procInfoSelectStat.Execute(Recursive);
}

std::vector<Preset> SqliteCRUD::SelectPresets(std::wstring Field, std::wstring Like, bool Recursive)
{
    PresetSelectStatement presetSelectStatement(Database, Field, Like);
    return presetSelectStatement.Execute(Recursive);
}
