#pragma once
#include "StatementBase.hpp"
class GenDeleteStatement :
    public StatementBase
{
public:
    GenDeleteStatement(sqlite3* Database, std::wstring Table);
    GenDeleteStatement() = default;
    ~GenDeleteStatement() = default;

    void Bind(sqlite3_int64 Id);
    sqlite3_int64 Execute();
};

