#include "SelectStatementBase.hpp"

SelectStatementBase::SelectStatementBase(sqlite3 * Database, std::wstring Table, std::wstring Field, std::wstring Like)
    : StatementBase(Database, L"SELECT * FROM " + Table + L" WHERE " + Field + L" LIKE \'" + Like + L"\';")
{
}
