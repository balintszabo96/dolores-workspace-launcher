#pragma once
#include "dolores.hpp"
#include "NarrativeLogger.hpp"

class Service
{
public:

    Service(
        const std::string &name,
        bool canStop,
        bool canShutDown,
        bool canPauseContinue
    );

    ~Service();

    void run();

    // Change the state of the service.
    // Can only be done while run() is running.
    void setState(DWORD state);

    // The convenience versions/
    void setStateRunning();
    void setStatePaused();

    // The stopping is more complicated, it also sets the exit code.
    // Which can be either general or a service-specific error code.
    // The success indication is the general code NO_ERROR.
    // Can be called only while run() is running.
    void setStateStopped(DWORD exitCode);
    void setStateStoppedSpecific(DWORD exitCode);

    // On the lengthy operations, periodically call this to tell the
    // controller that the service is not dead.
    // Can be called only while run() is running.
    void bump();

    // Can be used to set the expected length of long operations.
    // Also does the bump.
    // Can be called only while run() is running.
    void hintTime(DWORD msec);

    virtual void onStart(
        __in DWORD argc,
        __in_ecount(argc) LPSTR *argv
    );
    virtual void onStop();
    virtual void onPause();
    virtual void onContinue();
    virtual void onShutdown();

protected:

    // The callback for the service start.
    static void WINAPI serviceMain(
        __in DWORD argc,
        __in_ecount(argc) LPSTR *argv
    );

    // The callback for the requests.
    static void WINAPI serviceCtrlHandler(DWORD ctrl);
   
    // The internal version that expects the caller to already hold statusCr_ ????
    void setStateL(DWORD state);

    static Service *instance_;

    std::string name_;

    SERVICE_STATUS_HANDLE statusHandle_;
    SERVICE_STATUS status_;

private:
    Service();
    Service(const Service &);
    void operator=(const Service &);

};

