#include "PresetSelectStatement.hpp"
#include "ProcInfoSelectStatement.hpp"

PresetSelectStatement::PresetSelectStatement(sqlite3 *Database, std::wstring Field, std::wstring Like)
    : SelectStatementBase(Database, L"preset", Field, Like)
{
}

std::vector<Preset> PresetSelectStatement::Execute(bool Recursive)
{
    int status = SQLITE_ERROR;
    std::vector<Preset> presets;

    while ((status = sqlite3_step(Statement)) == SQLITE_ROW)
    {
        Preset preset(
            sqlite3_column_int64(Statement, 0),
            (const wchar_t*)sqlite3_column_text16(Statement, 1)
        );
        preset.ProcInfos = (Recursive) ? SelectProcInfosForPresetId(preset.Id, Recursive) : std::vector<ProcInfo>();
        presets.push_back(preset);
    }
    if (status != SQLITE_DONE)
    {
        throw std::exception(("sqlite3_step failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database) + " Statement: " + sqlite3_sql(Statement)).c_str());
    }

    return presets;
}

std::vector<ProcInfo> PresetSelectStatement::SelectProcInfosForPresetId(sqlite3_int64 PresetId, bool Recursive)
{
    ProcInfoSelectStatement *procInfoSelectStat = new ProcInfoSelectStatement(Database, L"preset_id", std::to_wstring(PresetId));
    auto procInfos = procInfoSelectStat->Execute(Recursive);
    delete(procInfoSelectStat);
    return procInfos;
}

