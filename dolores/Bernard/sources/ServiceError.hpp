#pragma once
#include "OutgoingPacket.hpp"
class ServiceError :
    public OutgoingPacket
{
public:
    ServiceError(std::wstring &Message);
    ServiceError() = default;
    ~ServiceError() = default;

    // Inherited via OutgoingPacket
    virtual rapidjson::GenericStringBuffer<rapidjson::UTF16<>> Serialize() override;
private:
    std::wstring Message;
};

