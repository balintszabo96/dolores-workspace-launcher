#pragma once
#include "OutgoingPacket.hpp"

class SendFeedbackStatus :
    public OutgoingPacket
{
public:
    SendFeedbackStatus(bool IsEnabled);
    SendFeedbackStatus() = default;
    ~SendFeedbackStatus() = default;

    bool IsEnabled;

    // Inherited via OutgoingPacket
    virtual rapidjson::GenericStringBuffer<rapidjson::UTF16<>> Serialize() override;

};

