#include "PacketFactory.hpp"
#include "RequestAllPresets.hpp"
#include "RequestDeletePresets.hpp"
#include "RequestSavePreset.hpp"
#include "ReportGuiExit.hpp"
#include "RequestSubmitMessage.hpp"
#include "RequestGetFeedbackStatus.hpp"
#include "RequestSetFeedbackStatus.hpp"

std::shared_ptr<IncomingPacket> PacketFactory::CreateMatchingPacket(std::wstring & NewType, JsonDocument& doc)
{
    if (RequestAllPresets::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestAllPresets>());
    }
    if (RequestDeletePresets::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestDeletePresets>(doc));
    }
    if (RequestSavePreset::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestSavePreset>(doc));
    }
    if (ReportGuiExit::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<ReportGuiExit>());
    }
    if (RequestSubmitMessage::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestSubmitMessage>(doc));
    }
    if (RequestGetFeedbackStatus::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestGetFeedbackStatus>());
    }
    if (RequestSetFeedbackStatus::IsMatching(NewType))
    {
        return std::static_pointer_cast<IncomingPacket>(std::make_shared<RequestSetFeedbackStatus>(doc));
    }

    return nullptr;
}
