#pragma once
#include "dolores.hpp"
#include "SqliteCRUD.hpp"

class IncomingPacket
{
public:
    IncomingPacket() = default;
    ~IncomingPacket() = default;

    virtual bool Execute() = 0;

    static bool IsMatching(std::wstring &NewType);
};

