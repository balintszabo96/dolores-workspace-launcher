#include "WindowSelectStatement.hpp"

WindowSelectStatement::WindowSelectStatement(sqlite3 * Database, std::wstring Field, std::wstring Like)
    :SelectStatementBase(Database, L"window", Field, Like)
{
}

std::vector<Window> WindowSelectStatement::Execute()
{
    int status = SQLITE_ERROR;
    std::vector<Window> windows;
    RECT rectangle = { 0 };

    while ((status = sqlite3_step(Statement)) == SQLITE_ROW)
    {
        rectangle.left = (LONG)sqlite3_column_int64(Statement, 4);
        rectangle.top = (LONG)sqlite3_column_int64(Statement, 5);
        rectangle.right = (LONG)sqlite3_column_int64(Statement, 6);
        rectangle.bottom = (LONG)sqlite3_column_int64(Statement, 7);
        windows.push_back(Window(
            sqlite3_column_int64(Statement, 0),
            rectangle,
            sqlite3_column_int(Statement, 2),
            (const wchar_t*)sqlite3_column_text16(Statement, 3),
            sqlite3_column_int(Statement, 8)));
    }
    if (status != SQLITE_DONE)
    {
        throw std::exception(("sqlite3_step failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database) + " Statement: " + sqlite3_sql(Statement)).c_str());
    }

    return windows;
}
