#pragma once
#include "OutgoingPacket.hpp"
#include "Preset.hpp"

class SendAllPresets :
    public OutgoingPacket
{
public:
    SendAllPresets(std::vector<Preset> &Presets);
    SendAllPresets() = default;
    ~SendAllPresets() = default;

    std::vector<Preset> Presets;

    // Inherited via OutgoingPacket
    virtual rapidjson::GenericStringBuffer<rapidjson::UTF16<>> Serialize() override;

};

