#pragma once
#include "dolores.hpp"
#include <mutex>

class NarrativeLogger
{
public:
    
    static NarrativeLogger* GetInstance();

    void Error(DWORD Code, std::wstring Function, std::wstring Message);
    void Error(std::wstring Message);
    void Error(std::string Message);
    void Info(DWORD Code, std::wstring Function, std::wstring Message);
    void Info(std::wstring Message);
    void Info(std::string Message);
    void Exception(std::string Message);

protected:
    NarrativeLogger();
    ~NarrativeLogger();
private:
    std::wofstream logFile;
    std::wstring Path = L"C:\\ProgramData\\bernard.log";
    std::mutex write_lock;

    void LogException(std::wstring Message);
    void SendException(std::wstring Message);
};

