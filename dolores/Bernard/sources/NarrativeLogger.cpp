#include "NarrativeLogger.hpp"
#include <ctime>
#include <algorithm>
#include <iterator> 
#include "ServiceError.hpp"

#define WESTWORLD_BORN 1900

std::wstring TimeStamp()
{
    std::time_t nowLongTime = std::time(0);
    std::tm formattedTime;

    if (!SUCCESS(localtime_s(&formattedTime, &nowLongTime)))
    {
        return L"TimeStamp_error";
    }

    return std::to_wstring(WESTWORLD_BORN + formattedTime.tm_year) + L"/" + std::to_wstring(formattedTime.tm_mon + 1) + L"/" + std::to_wstring(formattedTime.tm_mday) + \
         L" " + std::to_wstring(formattedTime.tm_hour) + L":" + std::to_wstring(formattedTime.tm_min) + L":" + std::to_wstring(formattedTime.tm_sec);
   
}

NarrativeLogger * NarrativeLogger::GetInstance()
{
    static NarrativeLogger *instance = new NarrativeLogger();
    return instance;
}

void NarrativeLogger::Error(DWORD Code, std::wstring Function, std::wstring Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    logFile << TimeStamp() << L" [ERROR] Function: " << Function.c_str() << L" Code: " << Code << L" Message: " << Message.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [ERROR] Function: " << Function.c_str() << L" Code: " << Code << L" Message: " << Message.c_str() << std::endl;
}

void NarrativeLogger::Error(std::wstring Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    logFile << TimeStamp() << L" [ERROR] " << Message.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [ERROR] " << Message.c_str() << std::endl;
}

void NarrativeLogger::Error(std::string Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    std::wstring ws(Message.begin(), Message.end());
    logFile << TimeStamp() << L" [ERROR] " << ws.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [ERROR] " << ws.c_str() << std::endl;
}

void NarrativeLogger::Info(DWORD Code, std::wstring Function, std::wstring Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    logFile << TimeStamp() << L" [INFO] Function: " << Function.c_str() << L" Code: " << Code << L" Message: " << Message.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [INFO] Function: " << Function.c_str() << L" Code: " << Code << L" Message: " << Message.c_str() << std::endl;
}

void NarrativeLogger::Info(std::wstring Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    logFile << TimeStamp() << L" [INFO] " << Message.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [INFO] " << Message.c_str() << std::endl;
}

void NarrativeLogger::Info(std::string Message)
{
    std::lock_guard<std::mutex> guard(write_lock);

    std::wstring ws(Message.begin(), Message.end());
    logFile << TimeStamp() << L" [INFO] " << ws.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [INFO] " << ws.c_str() << std::endl;
}

void NarrativeLogger::Exception(std::string Message)
{
    std::wstring ws;
    std::copy(Message.begin(), Message.end(), std::back_inserter(ws));
    LogException(ws);
    SendException(ws);
}

void NarrativeLogger::LogException(std::wstring Message)
{
    std::lock_guard<std::mutex> guard(write_lock);
    
    logFile << TimeStamp() << L" [EXCEPTION] " << Message.c_str() << std::endl;
    std::wcout << TimeStamp().c_str() << L" [EXCEPTION] " << Message.c_str() << std::endl;
}

void NarrativeLogger::SendException(std::wstring Message)
{
    ServiceError serverError(Message);
    serverError.Send();
}

NarrativeLogger::NarrativeLogger()
{
    logFile.open(Path);
}

NarrativeLogger::~NarrativeLogger()
{
    logFile.close();
}
