#include "Service.hpp"


Service *Service::instance_;

Service::Service(
    const std::string &name,
    bool canStop,
    bool canShutDown,
    bool canPauseContinue
): name_(name), statusHandle_(nullptr)
{
    // The service runs in its own process.
    status_.dwServiceType = SERVICE_WIN32_OWN_PROCESS;

    // The service is starting
    status_.dwControlsAccepted = 0;
    if (canStop)
    {
        status_.dwControlsAccepted |= SERVICE_ACCEPT_STOP;
    }
    if (canShutDown)
    {
        status_.dwControlsAccepted |= SERVICE_ACCEPT_SHUTDOWN;
    }
    if (canPauseContinue)
    {
        status_.dwControlsAccepted |= SERVICE_ACCEPT_PAUSE_CONTINUE;
    }

    status_.dwWin32ExitCode = NO_ERROR;
    status_.dwServiceSpecificExitCode = 0;
    status_.dwCheckPoint = 0;
    status_.dwWaitHint = 0;
}


Service::~Service()
{
}

void Service::run()
{
    instance_ = this;

    SERVICE_TABLE_ENTRY serviceTable[] =
    {
        {(LPSTR)name_.c_str(), serviceMain},
        {NULL, NULL}
    };

    if (!StartServiceCtrlDispatcher(serviceTable))
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"StartServiceCtrlDispatcher", L"Could not dispatch service table");
    }
}

void Service::setState(DWORD state)
{
    setStateL(state);
}


void Service::setStateL(DWORD state)
{
    status_.dwCurrentState = state;
    status_.dwCheckPoint = 0;
    status_.dwWaitHint = 0;
    SetServiceStatus(statusHandle_, &status_);
    NarrativeLogger::GetInstance()->Info(state, L"Service::setStateL", L"State set");
}

void Service::setStateRunning()
{
    setState(SERVICE_RUNNING);
}

void Service::setStatePaused()
{
    setState(SERVICE_PAUSED);
}

void Service::setStateStopped(DWORD exitCode)
{
    status_.dwWin32ExitCode = exitCode;
    setStateL(SERVICE_STOPPED);
}

void Service::setStateStoppedSpecific(DWORD exitCode)
{
    status_.dwWin32ExitCode = ERROR_SERVICE_SPECIFIC_ERROR;
    status_.dwServiceSpecificExitCode = exitCode;
    setStateL(SERVICE_STOPPED);
}

void Service::bump()
{
    ++status_.dwCheckPoint;
    SetServiceStatus(statusHandle_, &status_);
    NarrativeLogger::GetInstance()->Info(status_.dwCheckPoint, L"Service::bump", L"Sent a bump");
}

void Service::hintTime(DWORD msec)
{
    ++status_.dwCheckPoint;
    status_.dwWaitHint = msec;
    SetServiceStatus(statusHandle_, &status_);
    status_.dwWaitHint = 0;
    NarrativeLogger::GetInstance()->Info(msec, L"Service::hintTime", L"Sent hint time");
}

void Service::onStart(DWORD argc, LPSTR * argv)
{
    setState(SERVICE_RUNNING);
}

void Service::onStop()
{
    setStateStopped(NO_ERROR);
}

void Service::onPause()
{
    setState(SERVICE_PAUSED);
}

void Service::onContinue()
{
    setState(SERVICE_RUNNING);
}

void Service::onShutdown()
{
    onStop();
}

void Service::serviceMain(DWORD argc, LPSTR * argv)
{
    // Register the handler function for the service
    instance_->statusHandle_ = RegisterServiceCtrlHandler(
        instance_->name_.c_str(),
        serviceCtrlHandler
    );

    if (instance_->statusHandle_ == NULL)
    {
        NarrativeLogger::GetInstance()->Error(GetLastError(), L"RegisterServiceCtrlHandler", L"Registering control handle failed");
    }

    // Start the service
    instance_->setState(SERVICE_START_PENDING);
    instance_->onStart(argc, argv);
}

void Service::serviceCtrlHandler(DWORD ctrl)
{
    bool operationPerformed = false;
    switch (ctrl)
    {
    case SERVICE_CONTROL_STOP:
        if (instance_->status_.dwControlsAccepted & SERVICE_ACCEPT_STOP)
        {
            instance_->setState(SERVICE_STOP_PENDING);
            instance_->onStop();
            operationPerformed = true;
        }
        break;

    case SERVICE_CONTROL_PAUSE:
        if (instance_->status_.dwControlsAccepted & SERVICE_ACCEPT_PAUSE_CONTINUE)
        {
            instance_->setState(SERVICE_PAUSE_PENDING);
            instance_->onPause();
            operationPerformed = true;
        }
        break;

    case SERVICE_CONTROL_CONTINUE:
        if (instance_->status_.dwControlsAccepted & SERVICE_ACCEPT_PAUSE_CONTINUE)
        {
            instance_->setState(SERVICE_CONTINUE_PENDING);
            instance_->onContinue();
            operationPerformed = true;
        }
        break;

    case SERVICE_CONTROL_SHUTDOWN:
        if (instance_->status_.dwControlsAccepted & SERVICE_ACCEPT_SHUTDOWN)
        {
            instance_->setState(SERVICE_STOP_PENDING);
            instance_->onShutdown();
            operationPerformed = true;
        }
        break;

    case SERVICE_CONTROL_INTERROGATE:
        SetServiceStatus(instance_->statusHandle_, &instance_->status_);
        operationPerformed = true;
        break;

    default:
        break;
    }

    if (!operationPerformed)
    {
        NarrativeLogger::GetInstance()->Info(ctrl, L"Service::serviceCtrlHandler", L"Could not perform the requested operation");
    }
}
