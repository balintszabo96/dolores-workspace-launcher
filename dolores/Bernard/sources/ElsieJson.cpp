#include "ElsieJson.hpp"

rapidjson::GenericStringBuffer<rapidjson::UTF16<>> ElsieJson::Serialize(std::vector<Preset> &Presets)
{
    rapidjson::GenericStringBuffer<rapidjson::UTF16<>> stringBuffer;
    rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF16<>>, rapidjson::UTF16<>, rapidjson::UTF16<>> writer(stringBuffer);

    // start array of presets
    writer.StartArray();
    for (auto &preset : Presets)
    {
        // start one preset
        writer.StartObject();

        writer.Key(L"Id");
        writer.Int64(preset.Id);

        writer.Key(L"Name");
        writer.String(preset.Name.c_str());

        // start array of procinfos
        writer.Key(L"ProcInfos");
        writer.StartArray();

        for (auto &procInfo : preset.ProcInfos)
        {
            // start one procinfo
            writer.StartObject();

            writer.Key(L"Id");
            writer.Int64(procInfo.Id);

            writer.Key(L"Name");
            writer.String(procInfo.Name.c_str());

            writer.Key(L"ExecutablePath");
            writer.String(procInfo.ExecutablePath.c_str());

            writer.Key(L"CommandLine");
            writer.String(procInfo.CommandLine.c_str());

            // start array of window
            writer.Key(L"Windows");
            writer.StartArray();

            for (auto &window : procInfo.Windows)
            {
                // start one window
                writer.StartObject();

                writer.Key(L"Id");
                writer.Int64(window.Id);

                // start one rect
                writer.Key(L"Rectangle");
                writer.StartObject();

                writer.Key(L"left");
                writer.Int64(window.Rectangle.left);

                writer.Key(L"top");
                writer.Int64(window.Rectangle.top);

                writer.Key(L"right");
                writer.Int64(window.Rectangle.right);

                writer.Key(L"bottom");
                writer.Int64(window.Rectangle.bottom);

                // end one rect
                writer.EndObject();

                writer.Key(L"IsVisible");
                writer.Bool(window.IsVisible);

                writer.Key(L"Title");
                writer.String(window.Title.c_str());

                writer.Key(L"IsActive");
                writer.Bool(window.IsActive);

                // end one window
                writer.EndObject();
            }

            // end array of window
            writer.EndArray();

            // end one procinfo
            writer.EndObject();
        }

        // end array of procinfos
        writer.EndArray();


        // end one preset
        writer.EndObject();
    }
    // end array of presets
    writer.EndArray();

    return stringBuffer;
}

std::vector<Preset> ElsieJson::Deserialize(std::wstring & Json)
{
    std::vector<Preset> presets;
    rapidjson::GenericDocument<rapidjson::UTF16<>> doc;
    doc.Parse(Json.c_str());

    for (ULONG itPreset = 0; itPreset < doc.Size(); itPreset++)
    {
        Preset preset(
            doc[itPreset][L"Id"].GetInt64(),
            doc[itPreset][L"Name"].GetString()
        );

        if (doc[itPreset].HasMember(L"ProcInfos"))
        {
            auto docProcInfo = doc[itPreset][L"ProcInfos"].GetArray();
            for (ULONG itProcInfo = 0; itProcInfo < docProcInfo.Size(); itProcInfo++)
            {
                ProcInfo procInfo(
                    docProcInfo[itProcInfo][L"Id"].GetInt64(),
                    docProcInfo[itProcInfo][L"Name"].GetString(),
                    docProcInfo[itProcInfo][L"ExecutablePath"].GetString(),
                    docProcInfo[itProcInfo][L"CommandLine"].GetString()
                );
                
                if (docProcInfo[itProcInfo].HasMember(L"Windows"))
                {
                    auto docWindow = docProcInfo[itProcInfo][L"Windows"].GetArray();
                    for (ULONG itWindow = 0; itWindow < docWindow.Size(); itWindow++)
                    {
                        RECT rectangle;
                        rectangle.left = (LONG)docWindow[itWindow][L"Rectangle"][L"left"].GetInt64();
                        rectangle.top = (LONG)docWindow[itWindow][L"Rectangle"][L"top"].GetInt64();
                        rectangle.right = (LONG)docWindow[itWindow][L"Rectangle"][L"right"].GetInt64();
                        rectangle.bottom = (LONG)docWindow[itWindow][L"Rectangle"][L"bottom"].GetInt64();

                        Window window(
                            docWindow[itWindow][L"Id"].GetInt64(),
                            rectangle,
                            docWindow[itWindow][L"IsVisible"].GetBool(),
                            docWindow[itWindow][L"Title"].GetString(),
                            docWindow[itWindow][L"IsActive"].GetBool()
                        );

                        procInfo.Windows.push_back(window);
                    }
                }

                preset.ProcInfos.push_back(procInfo);
            }
        }

        presets.push_back(preset);
    }

    return presets;
}
