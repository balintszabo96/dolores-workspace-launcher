#include "RequestSetFeedbackStatus.hpp"
#include "SendFeedbackStatus.hpp"

RequestSetFeedbackStatus::RequestSetFeedbackStatus(JsonDocument & doc)
{
    IsEnabled = doc[L"IsEnabled"].GetBool();
}

bool RequestSetFeedbackStatus::IsMatching(std::wstring & NewType)
{
    return std::wstring(L"requestsetfeedbackstatus") == NewType;
}

bool RequestSetFeedbackStatus::Execute()
{
    SendFeedbackStatus sendFeedbackStatus(gFm->SetFeedbackStatus(IsEnabled));
    sendFeedbackStatus.Send();
    return true;
}
