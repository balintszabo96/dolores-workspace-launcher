#pragma once
#include "SqliteBase.hpp"

class StatementBase : public SqliteBase
{
public:
    StatementBase(sqlite3 *Database, std::wstring Query);
    StatementBase() = default;
    ~StatementBase();

    void Finalize();

protected:
    void Reset();
    std::wstring Query;
    sqlite3_stmt *Statement = nullptr;
};

