#pragma once

#include "dolores.hpp"

class Window
{
public:
    sqlite3_int64 Id;
    RECT Rectangle;
    bool IsVisible;
    std::wstring Title;
    bool IsActive;

    Window(RECT Rectangle, bool IsVisible, std::wstring Title);
    Window(sqlite3_int64 Id, RECT Rectangle, bool IsVisible, std::wstring Title, bool IsActive);
    ~Window() = default;
};

