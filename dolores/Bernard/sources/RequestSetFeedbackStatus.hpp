#pragma once
#include "IncomingPacket.hpp"

class RequestSetFeedbackStatus :
    public IncomingPacket
{
public:
    RequestSetFeedbackStatus(JsonDocument &doc);
    RequestSetFeedbackStatus() = default;
    ~RequestSetFeedbackStatus() = default;

    bool IsEnabled;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
};

