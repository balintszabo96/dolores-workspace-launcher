#include <cstdlib>
#include <cerrno>
#include "FeedbackManager.hpp"
#include "NarrativeLogger.hpp"
#include "HttpRequest.hpp"
#include "base64.h"
#include <windows.h>

FeedbackManager::FeedbackManager()
{
    feedbackConfig = std::make_shared<FeedbackConfig>(feedbackConfigFile);
}

void FeedbackManager::CheckQueue()
{
    WIN32_FIND_DATA fd;
    std::string fileName;
    std::string relativeFilePath;
    HANDLE hFind = INVALID_HANDLE_VALUE;

    try
    {
        hFind = FindFirstFile(ticketPattern.c_str(), &fd);
        if (hFind != INVALID_HANDLE_VALUE)
        {
            do
            {
                fileName = std::string(fd.cFileName);
                relativeFilePath = feedbackFolder + "/" + fileName;
                std::ifstream ifs(relativeFilePath);
                std::string message{ std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>() };
                ifs.close();
                if (200 != SubmitMessage(message))
                {
                    break;
                }
                NarrativeLogger::GetInstance()->Info("Submitted " + fileName);
                if (!DeleteFile(relativeFilePath.c_str()))
                {
                    NarrativeLogger::GetInstance()->Error("Could not delete " + fileName + " last error " + std::to_string(GetLastError()));
                }
            } while (FindNextFile(hFind, &fd));
            FindClose(hFind);
        }
    }
    catch (const std::exception& e)
    {
        NarrativeLogger::GetInstance()->Error(e.what());
    }
}

bool FeedbackManager::IsFeedbackEnabled()
{
    std::lock_guard<std::mutex> guard(feedbackConfigLock);

    return feedbackConfig->IsEnabled;
}

bool FeedbackManager::SetFeedbackStatus(bool status)
{
    std::lock_guard<std::mutex> guard(feedbackConfigLock);

    feedbackConfig->IsEnabled = status;
    feedbackConfig->Dump(feedbackConfigFile);
    return feedbackConfig->IsEnabled;
}

int FeedbackManager::SubmitMessage(std::string message)
{
    std::lock_guard<std::mutex> guard(feedbackConfigLock);

    if (!feedbackConfig->IsEnabled)
    {
        NarrativeLogger::GetInstance()->Error(L"Cannot submit ticket. Feedback is not enabled.");
        return 0;
    }

    if (feedbackConfig->Username.empty() || feedbackConfig->Password.empty())
    {
        RegisterProduct();
    }

    std::string userNamePassword = feedbackConfig->Username + ":" + feedbackConfig->Password;
    std::string encoded = base64_encode((const unsigned char*)userNamePassword.c_str(), userNamePassword.size());

    http::Request request(feedbackConfig->Url + ticketController);
    http::Response response = request.send(
        "POST",
        message,
        {
            "Content-Type: application/json",
            "Authorization: Basic " + encoded
        }
    );

    NarrativeLogger::GetInstance()->Info(L"code " + std::to_wstring(response.code));

    return response.code;
}

std::string ReadRegKey(HKEY key, std::string subKey, std::string valueName)
{
    LSTATUS status = ERROR_UNHANDLED_ERROR;
    HKEY hKey;
    char readValue[MAX_PATH] = { 0 };
    DWORD cbData = MAX_PATH;
    DWORD regType = REG_SZ;

    status = RegOpenKeyA(key, subKey.c_str(), &hKey);
    if (ERROR_SUCCESS != status)
    {
        throw std::exception((std::string("RegOpenKeyA failed with error ") + std::to_string(status)).c_str());
    }

    status = RegQueryValueExA(hKey, valueName.c_str(), NULL, &regType, (LPBYTE)readValue, &cbData);
    if (ERROR_SUCCESS != status)
    {
        throw std::exception((std::string("RegQueryValueExA failed with error ") + std::to_string(status)).c_str());
    }

    status = RegCloseKey(hKey);
    if (ERROR_SUCCESS != status)
    {
        throw std::exception((std::string("RegCloseKey failed with error ") + std::to_string(status)).c_str());
    }

    return std::string(readValue);
}

void FeedbackManager::RegisterProduct()
{
    std::string autoUser = "auto:auto";
    rapidjson::Document doc;
    std::string machineGuid = ReadRegKey(HKEY_LOCAL_MACHINE, machineGuidSubKey, machineGuidValueName);
    NarrativeLogger::GetInstance()->Info("Read machine guid " + machineGuid);
    http::Request request(feedbackConfig->Url + authenticationController);
    http::Response response = request.send(
        "PUT",
        "\"" + machineGuid + "\"",
        {
            "Content-Type: application/json",
            "Authorization: Basic " + base64_encode((const unsigned char*)autoUser.c_str(), autoUser.size())
        }
    );
    std::string responseJson(response.body.begin(), response.body.end());

    NarrativeLogger::GetInstance()->Info(L"code " + std::to_wstring(response.code));
    NarrativeLogger::GetInstance()->Info("Response: " + responseJson);
    doc.Parse(responseJson.c_str());

    feedbackConfig->Username = doc["userName"].GetString();
    feedbackConfig->Password = doc["password"].GetString();
    feedbackConfig->Dump(feedbackConfigFile);
}

