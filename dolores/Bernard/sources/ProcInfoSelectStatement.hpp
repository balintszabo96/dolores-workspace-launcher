#pragma once
#include "SelectStatementBase.hpp"
#include "ProcInfo.hpp"
#include "Window.hpp"

class ProcInfoSelectStatement :
    public SelectStatementBase
{
public:
    ProcInfoSelectStatement(sqlite3 *Database, std::wstring Field, std::wstring Like);
    ProcInfoSelectStatement() = default;
    ~ProcInfoSelectStatement() = default;

    std::vector<ProcInfo> Execute(bool Recursive);
private:
    std::vector<Window> SelectWindowsForProcInfoId(sqlite3_int64 ProcInfoId);
};

