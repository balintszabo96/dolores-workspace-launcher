#include "RequestGetFeedbackStatus.hpp"
#include "SendFeedbackStatus.hpp"

bool RequestGetFeedbackStatus::IsMatching(std::wstring & NewType) 
{
    return std::wstring(L"requestgetfeedbackstatus") == NewType;
}

bool RequestGetFeedbackStatus::Execute()
{
    SendFeedbackStatus sendFeedbackStatus(gFm->IsFeedbackEnabled());
    sendFeedbackStatus.Send();
    return true;
}
