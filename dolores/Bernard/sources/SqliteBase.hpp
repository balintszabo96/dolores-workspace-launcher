#pragma once
#include "dolores.hpp"
#include <sqlite3.h>

class SqliteBase
{
public:
    SqliteBase(sqlite3 *Database);
    SqliteBase() = default;
    ~SqliteBase() = default;

    void BindInt(sqlite3_stmt *Statement, int Index, int Value);
    void BindInt64(sqlite3_stmt *Statement, int Index, sqlite3_int64 Value);
    void BindWstring(sqlite3_stmt *Statement, int Index, std::wstring &Value);
protected:
    sqlite3 * Database;
};

