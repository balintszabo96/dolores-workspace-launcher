#pragma once
#include "StatementBase.hpp"

class SelectStatementBase :
    public StatementBase
{
public:
    SelectStatementBase(sqlite3* Database, std::wstring Table, std::wstring Field, std::wstring Like);
    SelectStatementBase() = default;
    ~SelectStatementBase() = default;
};

