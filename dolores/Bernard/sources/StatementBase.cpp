#include "StatementBase.hpp"

StatementBase::StatementBase(sqlite3 * Database, std::wstring Query) : SqliteBase(Database)
{
    this->Database = Database;
    this->Query = Query;
    auto status = sqlite3_prepare16_v2(Database, Query.c_str(), -1, &Statement, nullptr);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_prepare16_v2 failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
}

StatementBase::~StatementBase()
{
    Finalize();
}

void StatementBase::Finalize()
{
    sqlite3_finalize(Statement);
}

void StatementBase::Reset()
{
    auto status = sqlite3_clear_bindings(Statement);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_clear_bindings failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }

    status = sqlite3_reset(Statement);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_reset failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
}
