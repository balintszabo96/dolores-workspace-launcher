#include "Window.hpp"



Window::Window(RECT Rectangle, bool IsVisible, std::wstring Title)
{
    this->Rectangle = Rectangle;
    this->IsVisible = IsVisible;
    this->Title = Title;
}

Window::Window(sqlite3_int64 Id, RECT Rectangle, bool IsVisible, std::wstring Title, bool IsActive)
    :Window(Rectangle, IsVisible, Title)
{
    this->Id = Id;
    this->IsActive = IsActive;
}

