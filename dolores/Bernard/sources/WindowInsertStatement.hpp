#pragma once
#include "InsertStatementBase.hpp"
#include "Window.hpp"

class WindowInsertStatement : public InsertStatementBase
{
public:
    WindowInsertStatement(sqlite3 *Database);
    WindowInsertStatement() = default;
    ~WindowInsertStatement() = default;

    void Bind(sqlite3_int64 ProcInfoId, Window &Wnd);
};

