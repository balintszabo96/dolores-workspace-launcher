#pragma once
#include "IncomingPacket.hpp"

class RequestSubmitMessage :
    public IncomingPacket
{
public:
    RequestSubmitMessage(JsonDocument& doc);
    RequestSubmitMessage() = default;
    ~RequestSubmitMessage() = default;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
private:
    std::wstring Message;
};

