#pragma once
#include "dolores.hpp"
#include "Preset.hpp"
#include "ProcInfo.hpp"
#include "Window.hpp"
#include <sqlite3.h>
#include "WindowInsertStatement.hpp"
#include "PresetInsertStatement.hpp"
#include "ProcInfoInsertStatement.hpp"
#include "GenDeleteStatement.hpp"
#include "WindowSelectStatement.hpp"
#include "ProcInfoSelectStatement.hpp"
#include "PresetSelectStatement.hpp"

class SqliteCRUD
{
public:
    SqliteCRUD();
    ~SqliteCRUD();

    sqlite3_int64 InsertConfig(Preset &NewPreset);
    sqlite3_int64 InsertPreset(std::wstring PresetName);
    sqlite3_int64 InsertProcInfo(sqlite3_int64 PresetId, ProcInfo &PInfo);
    sqlite3_int64 InsertWindow(sqlite3_int64 ProcInfoId, Window &Wnd);
    void DeletePresetById(sqlite3_int64 PresetId);
    std::vector<Window> SelectWindows(std::wstring Field, std::wstring Like);
    std::vector<ProcInfo> SelectProcInfos(std::wstring Field, std::wstring Like, bool Recursive);
    std::vector<Preset> SelectPresets(std::wstring Field, std::wstring Like, bool Recursive);
private:
    sqlite3 *Database;
    std::wstring DatabasePath = L"codex.db";
    std::wstring DatabaseName = L"codex";
};