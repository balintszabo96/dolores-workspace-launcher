#include "Preset.hpp"

Preset::Preset(std::wstring Name)
{
    this->Name = Name;
}

Preset::Preset(sqlite3_int64 Id, std::wstring Name)
    :Preset(Name)
{
    this->Id = Id;
}

Preset::Preset(std::wstring Name, std::vector<ProcInfo>& ProcInfos)
    :Preset(Name)
{    
    this->ProcInfos = ProcInfos;
}
