#include "ProcInfo.hpp"

ProcInfo::ProcInfo(std::wstring Name, std::wstring Ep, std::wstring Cl)
{
    this->Name = Name;
    this->ExecutablePath = Ep;
    this->CommandLine = Cl;
}

ProcInfo::ProcInfo(DWORD ProcessId, std::wstring Name, std::wstring Ep, std::wstring Cl)
    :ProcInfo(Name, Ep, Cl)
{
    this->ProcessId = ProcessId;
}

ProcInfo::ProcInfo(sqlite3_int64 Id, std::wstring Name, std::wstring Ep, std::wstring Cl)
    : ProcInfo(Name, Ep, Cl)
{
    this->Id = Id;
}


ProcInfo::~ProcInfo()
{
}
