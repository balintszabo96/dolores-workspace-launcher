#pragma once
#include "InsertStatementBase.hpp"

class PresetInsertStatement :
    public InsertStatementBase
{
public:
    PresetInsertStatement(sqlite3* Database);
    PresetInsertStatement() = default;
    ~PresetInsertStatement() = default;

    void Bind(std::wstring &Name);
};

