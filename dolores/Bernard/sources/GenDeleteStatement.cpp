#include "GenDeleteStatement.hpp"

GenDeleteStatement::GenDeleteStatement(sqlite3 * Database, std::wstring Table) : StatementBase(Database, L"DELETE FROM " + Table + L" WHERE id = @id")
{
}

void GenDeleteStatement::Bind(sqlite3_int64 Id)
{
    Reset();
    BindInt64(Statement, 1, Id);
}

sqlite3_int64 GenDeleteStatement::Execute()
{
    auto status = sqlite3_step(Statement);
    if (status != SQLITE_DONE)
    {
        throw std::exception(("sqlite3_step failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database) + " Statement: " + sqlite3_sql(Statement)).c_str());
    }
    return sqlite3_last_insert_rowid(Database);
}
