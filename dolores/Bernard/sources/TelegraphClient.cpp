#include "TelegraphClient.hpp"
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

TelegraphClient::TelegraphClient()
{
}


TelegraphClient::~TelegraphClient()
{
}

void TelegraphClient::Send(std::wstring Message)
{
    std::string ipAddress = "127.0.0.1";
    int connResult = 0;
    int sendResult = 0;
    int len = ((int)Message.size() + 1) * sizeof(WCHAR);
    SOCKET sock;
    sockaddr_in hint;

    WSADATA wsaData;
    WORD ver = MAKEWORD(2, 2);
    int wsResult = WSAStartup(ver, &wsaData);
    if (!SUCCESS(wsResult))
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"WSAStartup", L"Cannot initialize winsock");
        return;
    }

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == INVALID_SOCKET)
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"socket", L"INVALID SOCKET");
        goto cleanupWSA;
    }
    NarrativeLogger::GetInstance()->Info(L"Client socket created");

    hint.sin_family = AF_INET;
    hint.sin_port = htons(gCfg->SendingPort);
    inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

    connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if (connResult == SOCKET_ERROR)
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"connect", L"Cannot connect to server");
        goto cleanupSocket;
    }
    NarrativeLogger::GetInstance()->Info(L"Connect done");
    
    sendResult = send(sock, (char*)Message.c_str(), len, 0);
    if (sendResult == SOCKET_ERROR)
    {
        NarrativeLogger::GetInstance()->Error(WSAGetLastError(), L"send", L"Send error");
        goto cleanupSocket;
    }
    NarrativeLogger::GetInstance()->Info(L"Send done");

cleanupSocket:
    closesocket(sock);
cleanupWSA:
    WSACleanup();

    return;
}
