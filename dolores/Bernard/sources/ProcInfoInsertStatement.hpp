#pragma once
#include "InsertStatementBase.hpp"
#include "ProcInfo.hpp"

class ProcInfoInsertStatement :
    public InsertStatementBase
{
public:
    ProcInfoInsertStatement(sqlite3* Database);
    ProcInfoInsertStatement() = default;
    ~ProcInfoInsertStatement() = default;

    void Bind(sqlite3_int64 PresetId, ProcInfo &PInfo);
};

