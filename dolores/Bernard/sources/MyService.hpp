#pragma once

#include "dolores.hpp"
#include "Service.hpp"
#include "TelegraphServer.hpp"
#include "TelegraphClient.hpp"

class MyService: public Service
{
public:
    
    // The exit code that will be set by the application thread on exit.
    DWORD ExitCode;

    MyService(
        __in const std::string &name
    );

    ~MyService();

    virtual void onStart(
        __in DWORD argc,
        __in_ecount(argc) LPSTR *argv
    );

    virtual void onStop();

    static void AppThreadFunction();

    static void FeedbackThreadFunction();

protected:
    
    // Handle to the thread that will execute the application
    HANDLE AppThreadHandle;
    HANDLE FeedbackThreadHandle;
};

