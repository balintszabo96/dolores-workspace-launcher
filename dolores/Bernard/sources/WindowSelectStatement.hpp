#pragma once
#include "SelectStatementBase.hpp"
#include "Window.hpp"

class WindowSelectStatement :
    public SelectStatementBase
{
public:
    WindowSelectStatement(sqlite3 *Database, std::wstring Field, std::wstring Like);
    WindowSelectStatement() = default;
    ~WindowSelectStatement() = default;

    std::vector<Window> Execute();
};

