#include "SendAllPresets.hpp"
#include "ElsieJson.hpp"

SendAllPresets::SendAllPresets(std::vector<Preset>& Presets) : OutgoingPacket(L"sendallpresets")
{
    this->Presets = Presets;
}

rapidjson::GenericStringBuffer<rapidjson::UTF16<>> SendAllPresets::Serialize()
{
    rapidjson::GenericStringBuffer<rapidjson::UTF16<>> stringBuffer;
    rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF16<>>, rapidjson::UTF16<>, rapidjson::UTF16<>> writer(stringBuffer);

    // start of whole
    writer.StartObject();

    writer.Key(L"Type");
    writer.String(Type.c_str());
    
    // start of details
    writer.Key(L"Details");

    // start array of presets
    writer.StartArray();
    for (auto &preset : Presets)
    {
        // start one preset
        writer.StartObject();

        writer.Key(L"Id");
        writer.Int64(preset.Id);

        writer.Key(L"Name");
        writer.String(preset.Name.c_str());

        // start array of procinfos
        writer.Key(L"ProcInfos");
        writer.StartArray();

        for (auto &procInfo : preset.ProcInfos)
        {
            // start one procinfo
            writer.StartObject();

            writer.Key(L"Id");
            writer.Int64(procInfo.Id);

            writer.Key(L"Name");
            writer.String(procInfo.Name.c_str());

            writer.Key(L"ExecutablePath");
            writer.String(procInfo.ExecutablePath.c_str());

            writer.Key(L"CommandLine");
            writer.String(procInfo.CommandLine.c_str());

            // start array of window
            writer.Key(L"Windows");
            writer.StartArray();

            for (auto &window : procInfo.Windows)
            {
                // start one window
                writer.StartObject();

                writer.Key(L"Id");
                writer.Int64(window.Id);

                // start one rect
                writer.Key(L"Rectangle");
                writer.StartObject();

                writer.Key(L"left");
                writer.Int64(window.Rectangle.left);

                writer.Key(L"top");
                writer.Int64(window.Rectangle.top);

                writer.Key(L"right");
                writer.Int64(window.Rectangle.right);

                writer.Key(L"bottom");
                writer.Int64(window.Rectangle.bottom);

                // end one rect
                writer.EndObject();

                writer.Key(L"IsVisible");
                writer.Bool(window.IsVisible);

                writer.Key(L"Title");
                writer.String(window.Title.c_str());

                writer.Key(L"IsActive");
                writer.Bool(window.IsActive);

                // end one window
                writer.EndObject();
            }

            // end array of window
            writer.EndArray();

            // end one procinfo
            writer.EndObject();
        }

        // end array of procinfos
        writer.EndArray();


        // end one preset
        writer.EndObject();
    }
    // end array of presets
    writer.EndArray();

    // end of details
    writer.EndObject();

    return stringBuffer;
}
