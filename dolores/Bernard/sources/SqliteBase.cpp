#include "SqliteBase.hpp"



SqliteBase::SqliteBase(sqlite3 *Database)
{
    this->Database = Database;
}

void SqliteBase::BindInt(sqlite3_stmt * Statement, int Index, int Value)
{
    auto status = sqlite3_bind_int(Statement, Index, Value);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_bind_int failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
}

void SqliteBase::BindInt64(sqlite3_stmt * Statement, int Index, sqlite3_int64 Value)
{
    auto status = sqlite3_bind_int64(Statement, Index, Value);    
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_bind_int64 failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
}

void SqliteBase::BindWstring(sqlite3_stmt * Statement, int Index, std::wstring & Value)
{
    auto status = sqlite3_bind_text16(Statement, Index, Value.c_str(), -1, SQLITE_TRANSIENT);
    if (!SUCCESS(status))
    {
        throw std::exception(("sqlite3_bind_text16 failed with Status: " + std::to_string(status) + " Message: " + sqlite3_errmsg(Database)).c_str());
    }
}
