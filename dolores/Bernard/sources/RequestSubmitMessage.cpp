#include "RequestSubmitMessage.hpp"
#include "NarrativeLogger.hpp"

RequestSubmitMessage::RequestSubmitMessage(JsonDocument & doc)
{
    rapidjson::GenericStringBuffer<rapidjson::UTF16<>> buffer;
    buffer.Clear();
    rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF16<>>, rapidjson::UTF16<>, rapidjson::UTF16<>> writer(buffer);
    doc[L"Details"].Accept(writer);
    Message = buffer.GetString();
    NarrativeLogger::GetInstance()->Info(Message);
}

bool RequestSubmitMessage::IsMatching(std::wstring & NewType)
{
    return std::wstring(L"requestsubmitmessage") == NewType;
}

bool RequestSubmitMessage::Execute()
{
    //gFm->SubmitMessage(Message);
    return true;
}
