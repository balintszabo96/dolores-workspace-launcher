#include "FeedbackConfig.hpp"
#include "document.h"
#include "writer.h"
#include "istreamwrapper.h"
#include <fstream>
#include "NarrativeLogger.hpp"

FeedbackConfig::FeedbackConfig(std::string FeedbackFileName)
{
    try
    {
        std::ifstream ifs(FeedbackFileName);
        rapidjson::IStreamWrapper isw(ifs);
        rapidjson::Document doc;
        doc.ParseStream(isw);

        IsEnabled = doc["IsEnabled"].GetBool();
        Username = doc["Username"].GetString();
        Password = doc["Password"].GetString();
        Url = doc["Url"].GetString();
        ifs.close();
    }
    catch (std::exception &e)
    {
        NarrativeLogger::GetInstance()->Exception(e.what());
    }
}

void FeedbackConfig::Dump(std::string FeedbackFileName)
{
    rapidjson::StringBuffer stringBuffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(stringBuffer);

    writer.StartObject();

    writer.Key("IsEnabled");
    writer.Bool(IsEnabled);

    writer.Key("Username");
    writer.String(Username.c_str());

    writer.Key("Password");
    writer.String(Password.c_str());

    writer.Key("Url");
    writer.String(Url.c_str());

    writer.EndObject();

    std::ofstream ofs;
    ofs.open(FeedbackFileName.c_str(), std::ofstream::out | std::ofstream::trunc);
    ofs << stringBuffer.GetString();
    ofs.close();
}
