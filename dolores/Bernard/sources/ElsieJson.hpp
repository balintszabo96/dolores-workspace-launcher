#pragma once

#include "dolores.hpp"
#include "Preset.hpp"
#include "ProcInfo.hpp"
#include "Window.hpp"

class ElsieJson
{
public:
    ElsieJson() = default;
    ~ElsieJson() = default;
    
    static rapidjson::GenericStringBuffer<rapidjson::UTF16<>> Serialize(std::vector<Preset> &Presets);
    static std::vector<Preset> Deserialize(std::wstring &Json);
};

