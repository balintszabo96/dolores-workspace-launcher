#pragma once
#include "dolores.hpp"
#include "ProcInfo.hpp"

class Preset
{
public:
    sqlite3_int64 Id;
    std::wstring Name;
    std::vector<ProcInfo> ProcInfos;

    Preset(std::wstring Name);
    Preset(sqlite3_int64 Id, std::wstring Name);
    Preset(std::wstring Name, std::vector<ProcInfo> &ProcInfos);
    Preset() = default;
    ~Preset() = default;

};

