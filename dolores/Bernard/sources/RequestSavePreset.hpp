#pragma once
#include "IncomingPacket.hpp"
class RequestSavePreset :
    public IncomingPacket
{
public:
    RequestSavePreset(JsonDocument &doc);
    RequestSavePreset() = default;
    ~RequestSavePreset() = default;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
private:
    Preset PresetToSave;
};

