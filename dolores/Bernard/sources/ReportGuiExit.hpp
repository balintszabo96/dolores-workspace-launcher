#pragma once
#include "IncomingPacket.hpp"
class ReportGuiExit :
    public IncomingPacket
{
public:
    ReportGuiExit() = default;
    ~ReportGuiExit() = default;

    static bool IsMatching(std::wstring &NewType);

    // Inherited via IncomingPacket
    virtual bool Execute() override;
};

