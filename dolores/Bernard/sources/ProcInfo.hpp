#pragma once

#include "dolores.hpp"
#include "Window.hpp"

class ProcInfo
{
public:
    sqlite3_int64 Id;
    DWORD ProcessId;
    std::wstring Name;
    std::wstring ExecutablePath;
    std::wstring CommandLine;
    std::vector<Window> Windows;

    ProcInfo(std::wstring Name, std::wstring Ep, std::wstring Cl);
    ProcInfo(DWORD ProcessId, std::wstring Name, std::wstring Ep, std::wstring Cl);
    ProcInfo(sqlite3_int64 Id, std::wstring Name, std::wstring Ep, std::wstring Cl);
    ProcInfo() = default;
    ~ProcInfo();
};