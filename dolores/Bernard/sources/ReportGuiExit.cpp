#include "ReportGuiExit.hpp"
#include "NarrativeLogger.hpp"

bool ReportGuiExit::IsMatching(std::wstring & NewType)
{
    return L"reportguiexit" == NewType;
}

bool ReportGuiExit::Execute()
{
    NarrativeLogger::GetInstance()->Info(L"Executing ReportGuiExit");
    return false;
}
