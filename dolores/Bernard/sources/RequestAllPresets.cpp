#include "RequestAllPresets.hpp"
#include "SendAllPresets.hpp"

bool RequestAllPresets::IsMatching(std::wstring & NewType)
{
    return std::wstring(L"requestallpresets") == NewType;
}

bool RequestAllPresets::Execute()
{
    NarrativeLogger::GetInstance()->Info(L"Executing RequestAllPresets");
    SqliteCRUD msc;
    auto presets = msc.SelectPresets(L"id", L"%", true);
    SendAllPresets SendAllPresets(presets);
    SendAllPresets.Send();
    return true;
}
