#pragma once
#include "dolores.hpp"

class TelegraphServer
{
public:
    TelegraphServer();
    ~TelegraphServer();

    void Listen();
    bool Handle(std::wstring &Message);
};

