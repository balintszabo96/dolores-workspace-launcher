#include "SendFeedbackStatus.hpp"


SendFeedbackStatus::SendFeedbackStatus(bool IsEnabled) : OutgoingPacket(L"sendfeedbackstatus")
{
    this->IsEnabled = IsEnabled;
}

rapidjson::GenericStringBuffer<rapidjson::UTF16<>> SendFeedbackStatus::Serialize()
{
    rapidjson::GenericStringBuffer<rapidjson::UTF16<>> stringBuffer;
    rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF16<>>, rapidjson::UTF16<>, rapidjson::UTF16<>> writer(stringBuffer);

    writer.StartObject();

    writer.Key(L"Type");
    writer.String(Type.c_str());

    writer.Key(L"Details");
    writer.Bool(IsEnabled);

    writer.EndObject();

    return stringBuffer;
}

