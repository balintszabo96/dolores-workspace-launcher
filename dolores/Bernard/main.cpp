#include "dolores.hpp"
#include "MyService.hpp"
#include "SqliteCRUD.hpp"
#include "NarrativeLogger.hpp"
#include "ElsieJson.hpp"
#include "FeedbackManager.hpp"
//#define _DEBUG

void DebugFunction2()
{
    gCfg = std::make_shared<Configuration>();
    NarrativeLogger::GetInstance()->Info(L"ListeningPort: " + std::to_wstring(gCfg->ListengingPort) + L" SendingPort: " + std::to_wstring(gCfg->SendingPort));
    TelegraphServer server;
    server.Listen();
}

void DebugFunction3()
{
    const wchar_t *ms = L"{\"Type\":\"requestdeletepreset\"}";
    std::wstring message(ms);
    TelegraphServer server;
    server.Handle(message);
}

void DebugFeedback()
{
    
}

int
__cdecl
main(
    __in long argc,
    __in_ecount(argc) PSTR argv[]
)
{
#ifdef _DEBUG
    DebugFeedback();
#else
    auto svc = std::make_shared<MyService>("MyService");
    svc->run();
#endif    
    getchar();
    return 0;
}
