#pragma once
#include "IncomingPacket.hpp"

class RequestGetFeedbackStatus :
    public IncomingPacket
{
public:
    RequestGetFeedbackStatus();
    ~RequestGetFeedbackStatus();
};

