--
-- File generated with SQLiteStudio v3.2.1 on Wed Sep 26 12:41:20 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: preset
DROP TABLE IF EXISTS preset;

CREATE TABLE preset (
    id   INTEGER PRIMARY KEY ASC AUTOINCREMENT
                 UNIQUE
                 NOT NULL,
    name STRING  NOT NULL
                 UNIQUE
);


-- Table: procinfo
DROP TABLE IF EXISTS procinfo;

CREATE TABLE procinfo (
    id              INTEGER PRIMARY KEY AUTOINCREMENT
                            UNIQUE
                            NOT NULL,
    preset_id       INTEGER REFERENCES preset (id) ON DELETE CASCADE
                                                   ON UPDATE CASCADE
                            NOT NULL,
    name            TEXT    NOT NULL,
    executable_path TEXT    NOT NULL,
    command_line    TEXT    NOT NULL
);


-- Table: window
DROP TABLE IF EXISTS window;

CREATE TABLE window (
    id          INTEGER PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    procinfo_id INTEGER REFERENCES procinfo (id) ON DELETE CASCADE
                                                 ON UPDATE CASCADE
                        NOT NULL,
    is_visible  BOOLEAN NOT NULL,
    title       STRING  NOT NULL,
    [left]      INTEGER NOT NULL,
    top         INTEGER NOT NULL,
    [right]     INTEGER NOT NULL,
    bottom      INTEGER NOT NULL,
    is_active   BOOLEAN NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
