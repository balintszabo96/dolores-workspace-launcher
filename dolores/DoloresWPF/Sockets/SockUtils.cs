﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DoloresWPF.Sockets
{
    public class SockUtils
    {
        static public IPAddress GetLoopbackAddress(IPHostEntry hostEntry)
        {
            IPAddress ipAddress = null;

            foreach (var address in hostEntry.AddressList)
            {
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipAddress = address;
                }
            }

            if (ipAddress == null)
            {
                throw new Exception("Loopback address not found");
            }

            return ipAddress;
        }
    }
}
