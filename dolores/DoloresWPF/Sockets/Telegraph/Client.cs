﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using DoloresWPF.Utils;

namespace DoloresWPF.Sockets.Telegraph
{
    public class Client
    {
        public static void Send(Socket Sender, string Message)
        {
            Send(Sender, Encoding.Unicode.GetBytes(Message));
        }
        public static void Send(Socket Sender, byte[] Bytes)
        {
            try
            {
                NarrativeLogger.Instance.Info(String.Format("Sending socket connected to {0}", Sender.RemoteEndPoint.ToString()));
                int bytesSent = Sender.Send(Bytes);
                NarrativeLogger.Instance.Info(String.Format("Sent {0} / {0}", bytesSent, Bytes.Count()));
            }
            catch(Exception ex)
            {
                NarrativeLogger.Instance.Error(String.Format("Exception: {0}", ex.ToString()));
            }
        }
    }
}
