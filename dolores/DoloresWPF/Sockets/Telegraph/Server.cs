﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Threading;
using DoloresWPF.Sockets.Packet.Incoming;
using DoloresWPF.Utils;
using DoloresWPF.Feedback;
using Newtonsoft.Json;

namespace DoloresWPF.Sockets.Telegraph
{
    public static class Server
    {
        public static string IncomingData = null;
        public static int MAX_BUFFER = 4096;
        public static void StartListening(int ListeningPort, MainWindow main)
        {
            byte[] incomingDataBuffer = new Byte[MAX_BUFFER];

            NarrativeLogger.Instance.Info("Starting up the server");

            try
            {
                IPHostEntry ipHostEntry = Dns.GetHostEntry("localhost");
                IPAddress ipAddress = SockUtils.GetLoopbackAddress(ipHostEntry);
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, ListeningPort);
                Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                string tokenData = null;


                listener.Bind(localEndPoint);
                listener.Listen(10);

                while (true)
                {
                    NarrativeLogger.Instance.Info("Waiting for client connections");
                    Socket handler = listener.Accept();
                    NarrativeLogger.Instance.Info("Connection accepted");

                    IncomingData = null;

                    while (true)
                    {
                        int nrBytesRecieved = handler.Receive(incomingDataBuffer);
                        tokenData = Encoding.Unicode.GetString(incomingDataBuffer, 0, nrBytesRecieved);
                        if (tokenData.Length == 0)
                        {
                            break;
                        }
                        IncomingData += tokenData;
                    }

                    Handle(IncomingData, main);

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    NarrativeLogger.Instance.Info("Connection closed");
                }
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
                Ticket.SubmitTicket(new ErrorTicket("DoloresWPF StartListening", ex));
            }
        }

        static void Handle(string message, MainWindow main)
        {
            Regex pattern = new Regex("{\"Type\":\"(?<type>.*)\",\"Details\":.*}");
            Match match = pattern.Match(message);
            string type = match.Groups["type"].Value;
            
            switch (type)
            {
                case "sendallpresets":
                    HandleGetAllPresets(message, main);
                    break;
                case "sendnewpreset":
                    HandleNewPreset(message, main);
                    break;
                case "sendfeedbackstatus":
                    HandleSendFeedbackStatus(message, main);
                    break;
                case "serviceerror":
                    HandleServiceError(message);
                    break;
                default:
                    break;
            }
        }

        static void HandleGetAllPresets(string Message, MainWindow main)
        {
            GetAllPresets getAllPresets = JsonConvert.DeserializeObject<GetAllPresets>(Message);
            main.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(
                () =>
                {
                    main.PresetsPage.PresetsListView.ItemsSource = getAllPresets.Details;
                    main.PresetsPage.PresetsListView.Items.Refresh();
                    main.RefreshPresetsMenuItems();
                }));
        }

        static void HandleNewPreset(string Message, MainWindow main)
        {
            GetNewPreset getNewPreset = JsonConvert.DeserializeObject<GetNewPreset>(Message);
        }

        static void HandleServiceError(string Message)
        {
            ServiceError error = JsonConvert.DeserializeObject<ServiceError>(Message);
            MessageBox.Show("Service error:\n" + error.Details);
        }

        static void HandleSendFeedbackStatus(string Message, MainWindow main)
        {
            GetFeedbackStatus getFeedbackStatus = JsonConvert.DeserializeObject<GetFeedbackStatus>(Message);
            main.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(
                () =>
                {
                    main.UpdateFeedbackStatus(getFeedbackStatus.Details);
                }));
        }
        private static void Invoke(this UIElement element, Action action)
        {
            element.Dispatcher.Invoke(action, null);
        }
    }
}
