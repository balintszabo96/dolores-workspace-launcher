﻿namespace DoloresWPF.Sockets.Packet.Incoming
{
    public class ServiceError
    {
        public string Type;
        public string Details;

        public ServiceError(string type, string details)
        {
            Type = type;
            Details = details;
        }

        public ServiceError()
        {
        }
    }
}
