﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoloresWPF.Sockets.Packet.Incoming
{
    public class GetFeedbackStatus
    {
        public string Type { get; set; }
        public bool Details { get; set; }

        public GetFeedbackStatus(string type, bool details)
        {
            Type = type;
            Details = details;
        }
        public GetFeedbackStatus()
        {
        }
    }
}
