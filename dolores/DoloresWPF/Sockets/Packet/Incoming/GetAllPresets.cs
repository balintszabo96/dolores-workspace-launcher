﻿using System.Collections.Generic;
using DoloresWPF.Objects;

namespace DoloresWPF.Sockets.Packet.Incoming
{
    public class GetAllPresets
    {
        public string Type;
        public IList<Preset> Details;

        public GetAllPresets(string type, IList<Preset> details)
        {
            Type = type;
            Details = details;
        }

        public GetAllPresets()
        {
        }
    }
}
