﻿using DoloresWPF.Objects;

namespace DoloresWPF.Sockets.Packet.Incoming
{
    public class GetNewPreset
    {
        public string Type;
        public Preset Details;

        public GetNewPreset(Preset details)
        {
            Details = details;
        }

        public GetNewPreset()
        {
        }
    }
}
