﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoloresWPF.Feedback;

namespace DoloresWPF.Sockets.Packet.Outgoing
{
    public class RequestSubmitMessage
    {
        public string Type = "requestsubmitmessage";
        public Ticket Details;

        public RequestSubmitMessage(Ticket details)
        {
            Details = details;
        }

        public RequestSubmitMessage()
        {
        }
    }
}
