﻿namespace DoloresWPF.Sockets.Packet.Outgoing
{
    public class RequestLoadPreset
    {
        public string Type = "requestloadpreset";
        public long Details;

        public RequestLoadPreset(long details)
        {
            Details = details;
        }

        public RequestLoadPreset()
        {
        }
    }
}
