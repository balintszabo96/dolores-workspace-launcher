﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoloresWPF.Sockets.Packet.Outgoing
{
    public class RequestSetFeedbackStatus
    {
        public string Type = "requestsetfeedbackstatus";
        public bool IsEnabled { get; set; }

        public RequestSetFeedbackStatus(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }
    }
}
