﻿using DoloresWPF.Objects;

namespace DoloresWPF.Sockets.Packet.Outgoing
{
    public class RequestSavePreset
    {
        public string Type = "requestsavepreset";
        public Preset Details;

        public RequestSavePreset(Preset details)
        {
            Details = details;
        }

        public RequestSavePreset()
        {
        }
    }
}
