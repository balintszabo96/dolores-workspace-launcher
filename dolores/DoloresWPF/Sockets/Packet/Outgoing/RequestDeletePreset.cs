﻿namespace DoloresWPF.Sockets.Packet.Outgoing
{
    public class RequestDeletePreset
    {
        public string Type = "requestdeletepreset";
        public long DeletePresetId;

        public RequestDeletePreset(long deletePresetId)
        {
            DeletePresetId = deletePresetId;
        }

        public RequestDeletePreset()
        {
        }
    }
}
