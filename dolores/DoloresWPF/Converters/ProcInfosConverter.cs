﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using DoloresWPF.Objects;

namespace DoloresWPF.Converters
{
    [ValueConversion(typeof(List<ProcInfo>), typeof(string))]
    public class ProcInfosConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(IEnumerable))
                throw new InvalidOperationException("The target must be a String");

            var procInfos = (List<ProcInfo>)value;
            var selectedTitles = new List<string>();
            foreach (ProcInfo procInfo in procInfos)
            {
                foreach (Window wnd in procInfo.Windows)
                {
                    if (wnd.IsActive)
                    {
                        selectedTitles.Add(wnd.Title);
                    }
                }
            }

            return selectedTitles;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
