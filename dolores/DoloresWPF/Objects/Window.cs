﻿using System.ComponentModel;

namespace DoloresWPF.Objects
{
    public class Window : INotifyPropertyChanged
    {
        private long _id;
        public long Id
        {
            get => _id;
            set
            {
                _id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Id"));
            }
        }

        private Rect _rectangle;

        public Rect Rectangle
        {
            get => _rectangle;
            set
            {
                _rectangle = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Rectangle"));
            }
        }

        private bool _isVisible;

        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                _isVisible = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsVisible"));
            }
        }

        private string _title;

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Title"));
            }
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsActive"));
            }
        }

        public Window(long id, Rect rectangle, bool isVisible, string title, bool isActive)
        {
            Id = id;
            Rectangle = rectangle;
            IsVisible = isVisible;
            Title = title;
            IsActive = isActive;
        }

        public Window(Rect rectangle, bool isVisible, string title, bool isActive)
        {
            Rectangle = rectangle;
            IsVisible = isVisible;
            Title = title;
            IsActive = isActive;
        }

        public Window()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }
}
