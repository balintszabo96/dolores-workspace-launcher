﻿namespace DoloresWPF.Objects
{
    public class SocketConfiguration
    {
        public int ListeningPort { get; set; }
        public int SendingPort { get; set; }
   
        public SocketConfiguration(int listeningPort, int sendingPort)
        {
            ListeningPort = listeningPort;
            SendingPort = sendingPort;
        }

        public SocketConfiguration()
        {
        }
    }
}
