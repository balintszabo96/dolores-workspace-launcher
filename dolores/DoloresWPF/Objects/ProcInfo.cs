﻿using System.Collections.Generic;
using System.ComponentModel;

namespace DoloresWPF.Objects
{
    public class ProcInfo : INotifyPropertyChanged
    {
        private long _id;

        public long Id
        {
            get => _id;
            set
            {
                _id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Id"));
            }
        }

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Name"));
            }
        }

        private string _executablePath;

        public string ExecutablePath
        {
            get => _executablePath;
            set
            {
                _executablePath = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ExecutablePath"));
            }
        }

        private string _commandLine;

        public string CommandLine
        {
            get => _commandLine;
            set
            {
                _commandLine = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CommandLine"));
            }
        }

        private IList<Window> _windows;

        public IList<Window> Windows
        {
            get => _windows;
            set
            {
                _windows = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Windows"));
            }
        }

        public ProcInfo(long id, string name, string executablePath, string commandLine, IList<Window> windows)
        {
            Id = id;
            Name = name;
            ExecutablePath = executablePath;
            CommandLine = commandLine;
            Windows = windows;
        }

        public ProcInfo(long id, string name, string executablePath, string commandLine)
        {
            Id = id;
            Name = name;
            ExecutablePath = executablePath;
            CommandLine = commandLine;
        }

        public ProcInfo(string name, string executablePath, string commandLine)
        {
            Name = name;
            ExecutablePath = executablePath;
            CommandLine = commandLine;
            Windows = new List<Window>();
        }

        public ProcInfo()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }
}
