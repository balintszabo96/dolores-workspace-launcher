﻿using System.Collections.Generic;
using System.ComponentModel;

namespace DoloresWPF.Objects
{
    public class Preset : INotifyPropertyChanged
    {
        public long Id;                
        private string _name;
        private IList<ProcInfo> _procInfos;
        public IList<ProcInfo> ProcInfos
        {
            get { return _procInfos; }
            set
            {
                _procInfos = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProcInfos"));
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Name"));
            }
        }

        public Preset(long id, string name, IList<ProcInfo> procInfos)
        {
            Id = id;
            Name = name;
            ProcInfos = procInfos;
        }

        public Preset(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public Preset(IList<ProcInfo> procInfos)
        {
            ProcInfos = procInfos;
        }

        public Preset()
        {
        }

        public override string ToString()
        {
            return Name;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
    }
}
