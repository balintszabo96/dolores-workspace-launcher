﻿using System.ComponentModel;
using System.Dynamic;

namespace DoloresWPF.Objects
{
    public class Rect : INotifyPropertyChanged
    {
        private long _left;

        public long Left
        {
            get => _left;
            set
            {
                _left = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Left"));       
            }
        }

        private long _top;

        public long Top
        {
            get => _top;
            set
            {
                _top = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Top"));
            }
        }

        private long _right;

        public long Right
        {
            get => _right;
            set
            {
                _right = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Right"));
            }
        }

        private long _bottom;

        public long Bottom
        {
            get => _bottom;
            set
            {
                _bottom = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Bottom"));
            }
        }

        public Rect(long left, long top, long right, long bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        public Rect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }
}
