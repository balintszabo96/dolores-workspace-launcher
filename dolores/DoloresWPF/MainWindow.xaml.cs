﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoloresWPF.Feedback;
using DoloresWPF.Objects;
using DoloresWPF.Pages;
using DoloresWPF.Sockets;
using DoloresWPF.Sockets.Packet.Outgoing;
using DoloresWPF.Sockets.Telegraph;
using DoloresWPF.Utils;
using Newtonsoft.Json;
using Application = System.Windows.Application;
using Window = System.Windows.Window;
using ContextMenu = System.Windows.Forms.ContextMenu;
using ListView = System.Windows.Controls.ListView;
using MenuItem = System.Windows.Forms.MenuItem;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;

namespace DoloresWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Socket _senderSocket;
        private Thread _listener;
        private IPEndPoint _remoteEp;
        private ContextMenu _contextMenu;
        private MenuItem _mIExit;
        private MenuItem _mINewPreset;
        private MenuItem _mILoadPreset;
        private MenuItem _mIArrangePreset;
        private MenuItem[] _mILoadPresetItems;
        private MenuItem[] _mIArrangePresetItems;
        private NotifyIcon _nIDolores;
        private IList<string> _filters;

        public SocketConfiguration SocketConfig;
        public PresetsPage PresetsPage;
        public EditPage EditPage;
        public OptionsPage OptionsPage;
        public AboutPage AboutPage;

        public MainWindow()
        {
            ConfigureExceptionHandlers();
            CheckAlreadyRunning();
            try
            {
                InitializeExceptions();
                InitializeNotifyIcon();
                InitializeDolores();
                PresetsPage = new PresetsPage();
                OptionsPage = new OptionsPage(_senderSocket);
                AboutPage = new AboutPage();
                InitializeGui();
                EditPage = new EditPage(_senderSocket, ContentFrame);
                ContentFrame.Content = PresetsPage;
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
                Ticket.SubmitTicket(new ErrorTicket("DoloresWPF MainWindow", ex));
            }
        }

        #region Initialization
        private void InitializeGui()
        {
            InitializeComponent();

            ButtonOpenMenu.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
        }

        private void InitializeNotifyIcon()
        {
            _nIDolores = new NotifyIcon();
            _nIDolores.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.nIDolores_MouseDoubleClick);

            _contextMenu = new ContextMenu();

            _mIExit = new MenuItem();
            _mIExit.Index = 3;
            _mIExit.Text = "Exit";
            _mIExit.Click += new System.EventHandler(this.mIExit_Click);

            _mINewPreset = new MenuItem();
            _mINewPreset.Index = 2;
            _mINewPreset.Text = "New preset";
            _mINewPreset.Click += new System.EventHandler(this.mINewPreset_Click);

            _mIArrangePreset = new MenuItem();
            _mIArrangePreset.Index = 1;
            _mIArrangePreset.Text = "Arrange preset";

            _mILoadPreset = new MenuItem();
            _mILoadPreset.Index = 0;
            _mILoadPreset.Text = "Load preset";

            _contextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { _mILoadPreset, _mIArrangePreset, _mINewPreset, _mIExit });
            _nIDolores.ContextMenu = _contextMenu;
        }

        private void InitializeDolores()
        {
            InitializeSockets();
            Client.Send(_senderSocket, JsonConvert.SerializeObject(new RequestAllPresets()));
        }

        private void InitializeSockets()
        {
            using (StreamReader file = File.OpenText("Bernard.config"))
            {
                var serializer = new JsonSerializer();
                this.SocketConfig = (SocketConfiguration)serializer.Deserialize(file, typeof(SocketConfiguration));
                var temp = SocketConfig.SendingPort;
                SocketConfig.SendingPort = SocketConfig.ListeningPort;
                SocketConfig.ListeningPort = temp;
            }

            _listener = new Thread(() => Server.StartListening(SocketConfig.ListeningPort, this));
            _listener.IsBackground = true;
            _listener.Start();

            IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = SockUtils.GetLoopbackAddress(ipHostInfo);

            _remoteEp = new IPEndPoint(ipAddress, SocketConfig.SendingPort);

            _senderSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Unspecified);
            _senderSocket.Connect(_remoteEp);

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
        }

        private void InitializeExceptions()
        {
            using (StreamReader file = File.OpenText("Filters.config"))
            {
                var json = file.ReadToEnd();
                _filters = JsonConvert.DeserializeObject<List<string>>(json);
            }
        }

        private void CheckAlreadyRunning()
        {
            string procName = Process.GetCurrentProcess().ProcessName;

            Process[] processes = Process.GetProcessesByName(procName);

            if (processes.Length > 1)
            {
                MessageBox.Show(procName + " is already running, it might be minimized in the system tray");
                System.Windows.Application.Current.Shutdown();
            }
        }
        #endregion

        #region GuiMethods
        public void RefreshPresetsMenuItems()
        {
            var menuItemLoadPresets = new List<MenuItem>();
            var menuItemArrangePresets = new List<MenuItem>();
            var currentIndex = 0;

            foreach (Preset preset in PresetsPage.PresetsListView.Items)
            {
                var menuItemLoadPreset = new MenuItem();
                menuItemLoadPreset.Index = currentIndex;
                menuItemLoadPreset.Text = preset.Name;
                menuItemLoadPreset.Click += (sender, e) => mILoadPreset_Click(sender, e, preset);

                menuItemLoadPresets.Add(menuItemLoadPreset);

                var menuItemArrangePreset = new MenuItem();
                menuItemArrangePreset.Index = currentIndex;
                menuItemArrangePreset.Text = preset.Name;
                menuItemArrangePreset.Click += (sender, e) => mIArrangePreset_Click(sender, e, preset);

                menuItemArrangePresets.Add(menuItemArrangePreset);

                currentIndex++;
            }

            _mILoadPresetItems = menuItemLoadPresets.ToArray();
            _mILoadPreset.MenuItems.Clear();
            _mILoadPreset.MenuItems.AddRange(_mILoadPresetItems);

            _mIArrangePresetItems = menuItemArrangePresets.ToArray();
            _mIArrangePreset.MenuItems.Clear();
            _mIArrangePreset.MenuItems.AddRange(_mIArrangePresetItems);
        }

        private void ActivateMainWindow()
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.WindowState = WindowState.Normal;
                this.Activate();
                if (this.WindowState == WindowState.Normal)
                {
                    this.ShowInTaskbar = true;
                    _nIDolores.Visible = false;
                }
            }
        }

        public void UpdateFeedbackStatus(bool isEnabled)
        {
            Ticket.IsFeedbackEnabled = isEnabled;
            OptionsPage.UpdateFeedbackStatus(isEnabled);
        }
        #endregion

        #region EventListeners
        private void OnProcessExit(object sender, EventArgs e)
        {
            try
            {
                Client.Send(_senderSocket, JsonConvert.SerializeObject(new ReportGuiExit()));
                _senderSocket.Shutdown(SocketShutdown.Both);
                _senderSocket.Close();
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
                Ticket.SubmitTicket(new ErrorTicket("DoloresWPF OnProcessExit", ex));
            }
        }
        private void ButtonOpenMenu_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
            ButtonCloseMenu.Visibility = Visibility.Visible;
        }

        private void ButtonCloseMenu_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = (ListBoxItem)(sender as ListView).SelectedItem;
            var selectedPreset = (Preset)PresetsPage.PresetsListView.SelectedItem;
            try
            {
                switch (item.Name)
                {
                    case "Home":
                        ContentFrame.Navigate(PresetsPage);
                        break;
                    case "Load":
                        if (selectedPreset == null)
                        {
                            System.Windows.MessageBox.Show("No preset selected");
                            return;
                        }

                        LoadPreset(selectedPreset);
                        break;
                    case "Arrange":
                        if (selectedPreset == null)
                        {
                            System.Windows.MessageBox.Show("No preset selected");
                            return;
                        }

                        ArrangePreset(selectedPreset);
                        break;
                    case "New":
                        NewPreset();
                        break;
                    case "Edit":
                        if (selectedPreset == null)
                        {
                            System.Windows.MessageBox.Show("No preset selected");
                            return;
                        }

                        EditPreset(selectedPreset);
                        break;
                    case "Delete":
                        if (selectedPreset == null)
                        {
                            System.Windows.MessageBox.Show("No preset selected");
                            return;
                        }

                        DeletePreset(selectedPreset);
                        break;
                    case "Options":
                        ContentFrame.Navigate(OptionsPage);
                        break;
                    case "About":
                        ContentFrame.Navigate(AboutPage);
                        break;
                }
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
                Ticket.SubmitTicket(new ErrorTicket("DoloresWPF UIElement_OnPreviewMouseLeftButtonUp", ex));
            }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Minimized)
            {
                _nIDolores.Icon = new Icon(System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/doloresicon_9TK_icon.ico")).Stream);
                _nIDolores.BalloonTipIcon = ToolTipIcon.Info;
                _nIDolores.BalloonTipText = "Dolores minimized to system tray";
                this.ShowInTaskbar = false;
                _nIDolores.Visible = true;
            }

            base.OnStateChanged(e);

            if (WindowState == System.Windows.WindowState.Minimized)
                _nIDolores.ShowBalloonTip(1000);
        }

        private void nIDolores_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ActivateMainWindow();
        }

        private void mIExit_Click(object Sender, EventArgs e)
        {
            this.Close();
        }

        private void mINewPreset_Click(object Sender, EventArgs e)
        {
            NewPreset();
        }

        private void mILoadPreset_Click(object Sender, EventArgs e, Preset preset)
        {
            LoadPreset(preset);
        }

        private void mIArrangePreset_Click(object Sender, EventArgs e, Preset preset)
        {
            ArrangePreset(preset);
        }
        #endregion

        #region OperationMethods
        private void LoadPreset(Preset preset)
        {
            bool bFoundActiveWindow = CognitiveScanner.Reproduce(preset);
            if (!bFoundActiveWindow)
            {
                MessageBox.Show("The selected preset has no active windows.\n" +
                                "When creating a new preset, check \"Active\" near the windows that you want restored.");
            }
        }

        private void ArrangePreset(Preset preset)
        {
            bool bFoundActiveWindow = CognitiveScanner.Reproduce(preset, true);
            if (!bFoundActiveWindow)
            {
                MessageBox.Show("The selected preset has no active windows.\n" +
                                "When creating a new preset, check \"Active\" near the windows that you want restored.");
            }
        }

        private void NewPreset()
        {
            ActivateMainWindow();
            var scanResult = CognitiveScanner.Scan(_filters);
            Preset preset = new Preset(scanResult);
            EditPage.EditPresetName.Text = "New preset";
            EditPage.EditListView.ItemsSource = preset.ProcInfos;
            ContentFrame.Navigate(EditPage);
        }

        private void EditPreset(Preset preset)
        {
            EditPage.EditPresetName.Text = preset.Name;
            EditPage.EditListView.ItemsSource = preset.ProcInfos;
            ContentFrame.Navigate(EditPage);
        }

        private void DeletePreset(Preset preset)
        {
            Sockets.Telegraph.Client.Send(_senderSocket,
                JsonConvert.SerializeObject(new RequestDeletePreset(preset.Id)));
        }
        #endregion

        #region ExceptionHandlers
        private void ConfigureExceptionHandlers()
        {
            System.Windows.Forms.Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Ticket.SubmitTicket(new ErrorTicket("DoloresWPF ThreadException", e.Exception.StackTrace));
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Ticket.SubmitTicket(new ErrorTicket("DoloresWPF UnhandledException", e.ExceptionObject.ToString()));
        }
        #endregion
    }
}
