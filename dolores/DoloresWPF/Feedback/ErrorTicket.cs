﻿using DoloresWPF.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DoloresWPF.Feedback
{
    public class ErrorTicket : Ticket
    {
        public ErrorTicket(string summary, string details) : base(2, summary,
            $"Exception: {details}\n DoloresLog: {ReadTodayLogs(@"C:\ProgramData\dolores.log")}\n BernardLog: {ReadTodayLogs(@"C:\ProgramData\bernard.log")}")
        {
        }

        public ErrorTicket(string summary, Exception exception) : base(2, summary,
            $"Exception: {exception.Message}\n{exception.StackTrace}\n DoloresLog: {ReadTodayLogs(@"C:\ProgramData\dolores.log")}\n BernardLog: {ReadTodayLogs(@"C:\ProgramData\bernard.log")}")
        {
        }

        static private string ReadTodayLogs(string logPath)
        {
            string logs = "";
            Regex dateTimeRegex = new Regex(@"\d{4}/\d{1,2}/\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}");
            DateTime referenceDateTime = DateTime.Now;
            DateTime logDateTime;

            try
            {
                using (var fileStream = new FileStream(logPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        Match m = dateTimeRegex.Match(line);
                        if (m.Success)
                        {
                            logDateTime = DateTime.Parse(m.Value);
                            if (logDateTime.Date == referenceDateTime.Date)
                            {
                                break;
                            }
                        }
                    }

                    logs += line + streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
            }

            return logs;
        }
    }
}
