﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Globalization;
using DoloresWPF.Utils;

namespace DoloresWPF.Feedback
{
    public class Ticket
    {
        public DateTime TimeStamp { get; set; }
        public int FormatVersion { get; set; }
        public int TypeId { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }
        public static bool IsFeedbackEnabled { get; set; } = true; // assume it is true, might crash before getting this info

        public Ticket(int typeId, string summary, string details)
        {
            TimeStamp = DateTime.Now;
            FormatVersion = 1;
            TypeId = typeId;
            Summary = summary;
            Details = details;
        }

        public void Dump()
        {
            var fileName = $"{TimeStamp.ToString("yyyy_MM_dd_HH_mm_ss_ff")}.ticket";
            var tempFileName = $"{fileName}.temp";
            var feedbackPath = Path.Combine(Directory.GetCurrentDirectory(), "feedback");
            var filePath = Path.Combine(feedbackPath, fileName);
            var tempFilePath = Path.Combine(feedbackPath, tempFileName);

            File.WriteAllText(tempFilePath, JsonConvert.SerializeObject(this));
            File.Move(tempFilePath, filePath);
        }

        public static void SubmitTicket(Ticket ticket)
        {
            if (IsFeedbackEnabled)
            {
                ticket.Dump();
            }
        }
    }
}
