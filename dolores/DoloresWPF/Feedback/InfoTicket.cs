﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoloresWPF.Feedback
{
    public class InfoTicket : Ticket
    {
        public InfoTicket(string summary, string details) : base(1, summary, details)
        {
        }
    }
}
