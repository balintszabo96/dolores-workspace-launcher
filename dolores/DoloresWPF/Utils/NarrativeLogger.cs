﻿using System;

namespace DoloresWPF.Utils
{
    public sealed class NarrativeLogger
    {
        private static string Path = "C:\\ProgramData\\dolores.log";
        private static NarrativeLogger instance = null;
        private static readonly object padlock = new object();
        private static readonly object writePadlock = new object();

        public static NarrativeLogger Instance
        {
            get
            {
                lock(padlock)
                {
                    if(instance == null)
                    {
                        instance = new NarrativeLogger();
                    }
                    return instance;
                }
            }
        }        
        public void Error(string message)
        {
            lock (writePadlock)
            {
                using (var file = new System.IO.StreamWriter(Path, true))
                {
                    string toLog = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " [ERROR] " + message;
                    file.WriteLine(toLog);
                    Console.WriteLine(toLog);
                }
            }
        }

        public void Exception(Exception exception)
        {
            lock (writePadlock)
            {
                using (var file = new System.IO.StreamWriter(Path, true))
                {
                    string toLog = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " [EXCEPTION] " + $"{exception.Message}\n{exception.StackTrace}";
                    file.WriteLine(toLog);
                    Console.WriteLine(toLog);
                }
            }
        }
        public void Info(string message)
        {
            lock (writePadlock)
            {
                using (var file = new System.IO.StreamWriter(Path, true))
                {
                    string toLog = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " [INFO] " + message;
                    file.WriteLine(toLog);
                    Console.WriteLine(toLog);
                }
            }
        }
    }
}
