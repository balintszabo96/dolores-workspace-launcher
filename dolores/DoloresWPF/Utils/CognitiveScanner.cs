﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using DoloresWPF.Objects;
using DoloresWPF.Winapi;
using System.Threading.Tasks;
using System.Management;
using System.Diagnostics;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using Rect = DoloresWPF.Objects.Rect;
using Window = DoloresWPF.Objects.Window;


namespace DoloresWPF.Utils
{
    public class CognitiveScanner
    {
        public static int MAX_TITLE = 500;
        public static int MAX_RETRIES_WAIT_FOR_PROCESS = 120;
        public static int MAX_RETRIES_WAIT_FOR_SIZE = 20;
        public static int MAX_RETRIES_WAIT_FOR_WINDOW = 200;
        public static int RETRY_SLEEP = 1000;
        public static IList<ProcInfo> Scan(IList<string> filters)
        {
            IDictionary<int, ProcInfo> pDict = new Dictionary<int, ProcInfo>();
            ProcInfo pInfo;
            IntPtr hCurrentWnd = IntPtr.Zero;
            Win32.RECTANGLE rect;
            int processId = 0;
            StringBuilder title = new StringBuilder(MAX_TITLE);

            while (true)
            {
                hCurrentWnd = Win32.FindWindowEx(IntPtr.Zero, hCurrentWnd, null, null);
                if (hCurrentWnd == IntPtr.Zero)
                {
                    break;
                }

                if (!Win32.IsWindowVisible(hCurrentWnd))
                {
                    continue;
                }

                Win32.GetWindowText(hCurrentWnd, title, MAX_TITLE);
                if (title.Length == 0)
                {
                    continue;
                }

                Win32.GetWindowThreadProcessId(hCurrentWnd, ref processId);

                Win32.GetWindowRect(hCurrentWnd, out rect);

                Window wnd = new Window(new Rect(rect.Left, rect.Top, rect.Right, rect.Bottom), true, title.ToString(), false);
                if (pDict.TryGetValue(processId, out pInfo))
                {

                    pInfo.Windows.Add(wnd);
                    pDict[processId] = pInfo;
                }
                else
                {
                    if (Query(processId, out pInfo))
                    {
                        if (!IsExcepted(pInfo, filters))
                        {
                            pInfo.Windows.Add(wnd);
                            pDict.Add(new KeyValuePair<int, ProcInfo>(processId, pInfo));
                        }
                    }
                }
            }

            return pDict.Values.ToList();
        }

        public static bool Query(int processId, out ProcInfo pInfo)
        {
            pInfo = null;
            try
            {
                string wmiQuery = string.Format("SELECT Name, ExecutablePath, CommandLine FROM Win32_Process WHERE ProcessId={0}", processId);
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(wmiQuery);
                ManagementObjectCollection collection = searcher.Get();
                List<ManagementObject> managementList = collection.Cast<ManagementObject>().ToList();

                if (managementList.Count != 1)
                {
                    throw new Exception("Could not query process");
                }
                pInfo = new ProcInfo(managementList[0]["Name"].ToString(), managementList[0]["ExecutablePath"].ToString(), managementList[0]["CommandLine"].ToString());
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static bool Reproduce(Preset preset, bool arrangeOnly=false)
        {
            IList<Thread> threads = new List<Thread>();
            bool bFoundActiveWindow = false;

            foreach (var procInfo in preset.ProcInfos)
            {
                if (HasActiveWindow(procInfo))
                {
                    Thread thread = new Thread(() => ReproduceOneProcess(procInfo, arrangeOnly));
                    thread.IsBackground = true;
                    thread.Start();
                    threads.Add(thread);
                    bFoundActiveWindow = true;
                }
            }

            return bFoundActiveWindow;
        }

        static bool HasActiveWindow(ProcInfo procInfo)
        {
            foreach (var window in procInfo.Windows)
            {
                if (window.IsActive)
                {
                    return true;
                }
            }

            return false;
        }

        static void ReproduceOneProcess(ProcInfo pInfo, bool arrangeOnly=false)
        {
            NarrativeLogger.Instance.Info("Reproducing " + pInfo.ExecutablePath);
            try
            {
                if (arrangeOnly)
                {
                    WindowOperation(pInfo, 0);
                    return;
                }

                NarrativeLogger.Instance.Info("Launching " + pInfo.ExecutablePath);

                Win32.STARTUPINFO startupInfo = new Win32.STARTUPINFO();
                Win32.PROCESS_INFORMATION processInfo = new Win32.PROCESS_INFORMATION();
                Win32.SECURITY_ATTRIBUTES se1 = new Win32.SECURITY_ATTRIBUTES();
                Win32.SECURITY_ATTRIBUTES se2 = new Win32.SECURITY_ATTRIBUTES();
                if (!Win32.CreateProcess($"\"{pInfo.ExecutablePath}\"", pInfo.CommandLine, ref se1, ref se2, false, 0,
                    IntPtr.Zero, null, ref startupInfo, out processInfo))
                {
                    if (!Win32.CreateProcess(null, pInfo.CommandLine, ref se1, ref se2, false, 0,
                        IntPtr.Zero, null, ref startupInfo, out processInfo))
                        throw new Exception("CreateProcess Failed");
                }
                var process = Process.GetProcessById((int)processInfo.dwProcessId);

                NarrativeLogger.Instance.Info("Created " + pInfo.ExecutablePath);
                process.WaitForInputIdle();
                NarrativeLogger.Instance.Info("Waited for " + pInfo.ExecutablePath);

                WindowOperation(pInfo, processInfo.dwProcessId);
            }
            catch (Exception ex)
            {
                NarrativeLogger.Instance.Exception(ex);
            }
        }

        static void WindowOperation(ProcInfo pInfo, uint pid)
        {
            foreach (var window in pInfo.Windows)
            {
                if (window.IsActive)
                {
                    NarrativeLogger.Instance.Info("Adjusting frame for: " + window.Title);
                    AdjustWindow(window, pid);
                }
            }
        }

        static IntPtr GetProcessWindowOneIteration(Window wnd, uint newProcessId)
        {
            IList<IntPtr> possibleWindows = new List<IntPtr>();
            IntPtr hCurrentWnd = IntPtr.Zero;
            int processId = 0;
            StringBuilder title = new StringBuilder(MAX_TITLE);

            while (true)
            {
                hCurrentWnd = Win32.FindWindowEx(IntPtr.Zero, hCurrentWnd, null, null);
                if (hCurrentWnd == IntPtr.Zero)
                {
                    break;
                }                

                Win32.GetWindowText(hCurrentWnd, title, MAX_TITLE);
                if (title.ToString().Contains(wnd.Title))
                {
                    possibleWindows.Add(hCurrentWnd);
                }
            }

            switch (possibleWindows.Count)
            {
                case 0:
                    return IntPtr.Zero;
                case 1:
                    return possibleWindows[0];
                default:
                    foreach (var possibleWindow in possibleWindows)
                    {
                        Win32.GetWindowThreadProcessId(possibleWindow, ref processId);
                        if (newProcessId == processId)
                        {
                            return possibleWindow;
                        }
                    }
                    return IntPtr.Zero;
            }            
        }

        static IntPtr GetProcessWindow(Window wnd, uint newProcessId)
        {
            IntPtr hTargetWnd = IntPtr.Zero;    
            
            for (var retriesProcess = 0; retriesProcess < MAX_RETRIES_WAIT_FOR_PROCESS; retriesProcess++)
            {
                hTargetWnd = GetProcessWindowOneIteration(wnd, newProcessId);
                if (IntPtr.Zero != hTargetWnd)
                {
                    break;
                }
                Thread.Sleep(RETRY_SLEEP);
            }

            return hTargetWnd;
        }

        static void SetWindowPosition(IntPtr targetWindow, Window wnd)
        {
            Win32.RECTANGLE rect;
            int retriesSize = 0;
            int x, y, cx, cy;

            CalculateLocation(wnd.Rectangle, out x, out y, out cx, out cy);

            Win32.GetWindowRect(targetWindow, out rect);
            
            do
            {
                Win32.SetWindowPos(targetWindow, 0, x, y, cx, cy, Win32.SWP_SHOWWINDOW);
                Thread.Sleep(RETRY_SLEEP);
                Win32.GetWindowRect(targetWindow, out rect);
                retriesSize++;
            } while ((rect.Left != wnd.Rectangle.Left ||
                      rect.Top != wnd.Rectangle.Top ||
                      rect.Right != wnd.Rectangle.Right ||
                      rect.Bottom != wnd.Rectangle.Bottom) &&
                     retriesSize < MAX_RETRIES_WAIT_FOR_SIZE);

            if (retriesSize > MAX_RETRIES_WAIT_FOR_SIZE)
            {
                NarrativeLogger.Instance.Info($"Unable to set positions for {wnd.Title}: " +
                                              $"Left: {rect.Left}/{wnd.Rectangle.Left}, " +
                                              $"Top: {rect.Top}/{wnd.Rectangle.Top}, " +
                                              $"Right: {rect.Right}/{wnd.Rectangle.Right}, " +
                                              $"Bottom: {rect.Bottom}/{wnd.Rectangle.Bottom}");
            }
            else
            {
                NarrativeLogger.Instance.Info($"Set position for {wnd.Title}");
            }
        }

        static void AdjustWindow(Window wnd, uint newProcessId)
        {
            IntPtr hTargetWnd = GetProcessWindow(wnd, newProcessId);
            if (IntPtr.Zero == hTargetWnd)
            {
                NarrativeLogger.Instance.Info("Window title not found: " + wnd.Title);
                return;
            }

            SetWindowPosition(hTargetWnd, wnd);
        }

        static void CalculateLocation(Rect rectangle, out int x, out int y, out int cx, out int cy)
        {
            x = (int)rectangle.Left;
            y = (int)rectangle.Top;
            cx = (int)rectangle.Right - x;
            cy = (int)rectangle.Bottom - y;
        }

        static bool IsExcepted(ProcInfo pInfo, IList<string> filters)
        {
            foreach (var filter in filters)
            {
                if (pInfo.ExecutablePath.Contains(filter))
                {
                    return true;
                }
            }
            return false;
        }
    }

}
