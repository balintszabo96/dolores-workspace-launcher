﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using DoloresWPF.Objects;
using DoloresWPF.Sockets.Packet.Outgoing;
using DoloresWPF.Sockets.Telegraph;
using Newtonsoft.Json;

namespace DoloresWPF.Pages
{
    /// <summary>
    /// Interaction logic for EditPage.xaml
    /// </summary>
    public partial class EditPage : Page
    {
        private readonly Socket _senderSocket;
        private readonly Frame _contentFrame;
        public EditPage(Socket senderSocket, Frame contentFrame)
        {
            InitializeComponent();

            _senderSocket = senderSocket;
            _contentFrame = contentFrame;
        }

        private void SavePreset_OnClick(object sender, RoutedEventArgs e)
        {
            Preset preset = new Preset((IList<ProcInfo>)EditListView.ItemsSource)
            {
                Name = EditPresetName.Text
            };
            Client.Send(_senderSocket, JsonConvert.SerializeObject(new RequestSavePreset(preset)));
            _contentFrame.GoBack();
        }
    }
}
