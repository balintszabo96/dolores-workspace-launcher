﻿using DoloresWPF.Sockets.Packet.Outgoing;
using DoloresWPF.Sockets.Telegraph;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoloresWPF.Utils;

namespace DoloresWPF.Pages
{
    /// <summary>
    /// Interaction logic for OptionsPage.xaml
    /// </summary>
    public partial class OptionsPage : Page
    {
        private readonly Socket _senderSocket;
        public OptionsPage(Socket senderSocket)
        {
            InitializeComponent();

            _senderSocket = senderSocket;
            Client.Send(_senderSocket, JsonConvert.SerializeObject(new RequestGetFeedbackStatus()));
        }

        public void UpdateFeedbackStatus(bool isEnabled)
        {
            FeedbackToggle.IsChecked = isEnabled;
        }

        private void FeedbackToggle_OnChecked(object sender, RoutedEventArgs e)
        {
            Client.Send(_senderSocket, JsonConvert.SerializeObject(new RequestSetFeedbackStatus(true)));
        }

        private void FeedbackToggle_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Client.Send(_senderSocket, JsonConvert.SerializeObject(new RequestSetFeedbackStatus(false)));
        }
    }
}
