﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoloresWPF.Feedback;

namespace DoloresWPF.Pages
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Page
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        private void SendFeedbackButton_OnClick(object sender, RoutedEventArgs e)
        {
            string details = $"Rating: {StarRatingBar.Value}/{StarRatingBar.Max}; Message: {FeedbackTextArea.Text}";

            if (!Ticket.IsFeedbackEnabled)
            {
                MessageBox.Show("Please enable the Feedback option from the Options tab");
                return;
            }
            Ticket.SubmitTicket(new InfoTicket("User feedback", details));
            MessageBox.Show("Thank you! Your feedback is greatly appreciated :)");
        }
    }
}
