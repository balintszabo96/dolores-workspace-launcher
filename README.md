# dolores-workspace-launcher

Dolores is a "workspace launcher" application which has the purpose of:
*  recording your current workspace
*  reproducing it by launching the applications belonging to the recorded 
workspace and trying to place them at the appropiate screen location

For every application, it saves into an SQLite database the following data:
*  executable path of the application
*  command line of the application
*  title and position for every window of the application

The following GIFs should provide a short tutorial on how to use the application

**1.  Recording notepad.exe as the only application in the workspace (Note that
the command line is needed in order to open the same file again. This is why
I opened it in the first place from cmd.exe)**

![](gifs/notepadgif.gif)


**2.  Adding Total Commander to the workspace (the kind of application that does
not require a command line)**

![](gifs/totalcmdandnotepad.gif)



**3.  Dolores can be minimized to the System Tray and it can be used from there**

![](gifs/totalcmdandnotepadfromtray.gif)



**4.  A preset can be edited. Here I am removing Total Commander from the preset**

![](gifs/onlynotepad.gif)



**5.  Presets can be deleted**

![](gifs/delete.gif)

**6. Another example of save restore**

![](gifs/load_demo.gif)

**7. An example of arrange (application are already running and the windows are
arranged)**

![](gifs/arrange_demo.gif)

**8. Submitting user feedback (needs to have the Feedback option turned on)**

![](gifs/feedback_demo.gif)

NOTE: For anyone giving me feedback for my license thesis, it would be nice
if you could copy paste the following in the text area (where I wrote "Hello, 
this is my feedback") and give feedback based on these criteria:

**Name:** (only needed to check if I received the feedback from you)

**Installation(1(smallest) to 5(biggest)):**

**Useability(1 to 5):**

**Save and restore reliability(1 to 5):**

**Will I use it in the future(probability 1 to 5):**

**Any other comments:**


32 and 64 bit installers can be downloaded from 
https://gitlab.com/balintszabo96/dolores-workspace-launcher/tags